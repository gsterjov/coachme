from autobahn.wamp import exportSub, exportPub

from models import User


class WhiteboardService:

	def __init__ (self, user):
		self.user = user


	@exportSub ("whiteboard", True)
	def subscribe (self, prefix, suffix):
		'''
		Subscribe to whiteboard events if the user has access
		to the conference.
		'''
		id, suffix = suffix.lstrip('/').split('/', 1)

		user = User (self.user.id)
		return id == user.joined_conference.hget()


	@exportPub ("whiteboard", True)
	def publish (self, prefix, suffix, event):
		'''
		Publish a whiteboard event if the user is permitted to do so.
		'''
		id, suffix = suffix.lstrip('/').split('/', 1)

		user = User (self.user.id)
		if id == user.joined_conference.hget():
			return event

		return None