import uuid
from datetime import datetime

from autobahn.wamp import exportRpc, exportSub, exportPub

from models import User, Conference


class ConferenceService:

	def __init__ (self, server, user):
		self.server = server
		self.user = user
		self.contacts = set([contact.id for contact in user.contacts])


	@exportSub ("user", True)
	def subscribe (self, prefix, suffix):
		'''
		Subscribe to conference events directed at the specified user.
		'''
		user_id, suffix = suffix.lstrip('/').split('/', 1)
		return int(user_id) == self.user.id


	@exportSub ("conference", True)
	def subscribe (self, prefix, suffix):
		'''
		Subscribe to conference events directed at the specified user.
		'''
		id, suffix = suffix.lstrip('/').split('/', 1)

		conference = Conference (id)
		if self.user.id in conference.invited.smembers():
			return True

		return False


	@exportPub ("conference", True)
	def publish (self, prefix, suffix, event):
		'''
		Publish a conference event to the specified user if the current
		user is permitted to do so.
		'''
		id, suffix = suffix.lstrip('/').split('/', 1)

		conference = Conference (id)
		if self.user.id in conference.joined.smembers():
			return event

		return None



	@exportRpc
	def create (self, name, contacts):
		'''
		Create a conference and invite the specified people if the user
		is permitted to do so.
		'''
		contacts = set(contacts)

		# not permitted to invite all contacts
		difference = contacts - self.contacts
		permitted = len(difference) == 0

		contacts.add (self.user.id)

		if permitted:
			# get users
			users = User.get_or_create (contacts)

			conference = Conference(
				owner = self.user.id,
				name = name,
				invited = users,
			)

			# send invite events to all contacts
			invite = {
				"conference": conference.id.get(),
				"name": name,
				"owner": self.user.id
			}

			for contact in contacts:
				if not contact == self.user.id:
					uri = "http://coachme.com.au/conference/user/{0}/events#invite".format (contact)
					self.server.dispatch (uri, invite)

			return conference.id.get()

		return None


	@exportRpc
	def join (self, id):
		'''
		Join the specified conference.
		Can only be in one conference at a time.
		'''
		user = User (self.user.id)

		conference = Conference (id)
		user.joined_conference.hset (conference)

		uri = "http://coachme.com.au/conference/{0}/events#join".format (id)
		self.server.dispatch (uri, self.user.id)

		ret = {
			"id": id,
			"invites": list(conference.invited.smembers()),
			"joined": list(conference.joined()),
		}
		ret.update (conference.hgetall())
		return ret


	@exportRpc
	def leave (self):
		'''
		Leave the specified conference.
		'''
		user = User (self.user.id)
		conference = Conference (id)
		conference.joined.srem (user)

		uri = "http://coachme.com.au/conference/{0}/events#leave".format (id)
		self.server.dispatch (uri, self.user.id)

		return True


	@exportRpc
	def invites (self):
		'''
		Get a list of conferences the user has been invited to.
		'''
		conferences = {}
		user = User.get_or_create (self.user.id)

		for conference in user.invited_conferences().instances():
			id = conference.id.get()

			conferences[id] = {
				"id": id,
				"invites": list(conference.invited.smembers()),
				"joined": list(conference.joined()),
			}

			conferences[id].update (conference.hgetall())

		return conferences