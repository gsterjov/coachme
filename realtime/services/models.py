from limpyd import model
from limpyd.exceptions import DoesNotExist
from limpyd.contrib import related
from limpyd.contrib.database import PipelineDatabase

from django.conf import settings


connection = settings.REDIS_CONNECTIONS["default"]
main_database = PipelineDatabase (
	host = connection["HOST"],
	port = int(connection["PORT"]),
	db = 0
)


class BaseModel (related.RelatedModel):
	'''
	A related model using a pipeline database.
	'''
	database = main_database

	def __repr__ (self):
		return "<{0}: {1}>".format (self.__class__.__name__, self.__str__())



class User (BaseModel):
	'''
	The user model stores data related to the specified user.
	'''
	id = model.PKField()
	online = model.InstanceHashField()
	joined_conference = related.FKInstanceHashField ("Conference", related_name="joined")


	@staticmethod
	def get_or_create (ids):
		'''
		Retrieve existing users or create a blank one if they cannot
		be found in the cache.
		'''
		# single id
		if type(ids) is int:
			try:
				return User (ids)
			except DoesNotExist:
				return User (id=ids)

		# multiple ids
		else:
			ids = set(ids)

			# get existing users
			users = [user for user in User.collection().intersect(ids).instances()]
			existing = set([int(user.id.get()) for user in users])

			# create any missing users
			for id in ids - existing:
				users.append (User(id=id))

			return users


	def __str__ (self):
		return "id:{0} online:{1}".format (self.id.get(), self.online.hget())



class Conference (BaseModel):
	'''
	Stores publicly visible conference data.
	'''
	id = model.AutoPKField()
	name = model.InstanceHashField()

	owner = related.FKInstanceHashField ("User")
	invited = related.M2MSetField ("User", related_name="invited_conferences")


	def __str__ (self):
		return "id:{0} name:'{1}' owner:'{2}'".format (self.id.get(), *self.hmget("name", "owner"))