from datetime import datetime

from autobahn.wamp import exportSub, exportPub

from models import User


class ChatService:

	def __init__ (self, user):
		self.user = user
		self.contacts = [contact.id for contact in user.contacts]


	@exportSub ("chat", True)
	def subscribe (self, prefix, suffix):
		'''
		Subscribe to chat events directed at the specified user.
		'''
		user_id, suffix = suffix.lstrip('/').split('/', 1)
		permitted = int(user_id) == self.user.id

		if permitted:
			user = User.get_or_create (int(user_id))
			user.online.hset (True)

		return permitted


	@exportPub ("chat", True)
	def publish (self, prefix, suffix, event):
		'''
		Publish a chat event to the specified user if the current
		user is permitted to do so.
		'''
		user_id, suffix = suffix.lstrip('/').split('/', 1)
		permitted = int(user_id) in self.contacts

		# inject the sender and timestamp
		payload = {
			"sender": self.user.id,
			"ts": datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
			"message": event["message"],
		}

		return payload if permitted else None