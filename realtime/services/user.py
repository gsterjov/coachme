from autobahn.wamp import exportRpc

from limpyd.exceptions import DoesNotExist

from models import User


class UserService:

	def __init__ (self, user):
		self.user = user


	@exportRpc
	def details (self, id=None):
		'''
		Get the specified user details.
		'''
		# user id. get all information
		if id is None or id == self.user.id:
			user = User.get_or_create (self.user.id)
			details = {
				"id": user.id.get()
			}
			details.update (user.hgetall())
			return details

		# contact id. limit information
		else:
			try:
				user = User (id)
				return {
					"id": user.id.get(),
					"online": user.online.hget(),
				}
			except DoesNotExist:
				return {}