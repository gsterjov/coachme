from autobahn.wamp import exportSub, exportPub

from models import User


class RTCService:

	def __init__ (self, user):
		self.user = user
		self.contacts = set([contact.id for contact in user.contacts])


	@exportSub ("rtc", True)
	def subscribe (self, prefix, suffix):
		'''
		Subscribe to RTC signaling events.
		'''
		id, suffix = suffix.lstrip('/').split('/', 1)
		return int(id) == self.user.id


	@exportPub ("rtc", True)
	def publish (self, prefix, suffix, event):
		'''
		Publish RTC events to a user channel.
		'''
		id, suffix = suffix.lstrip('/').split('/', 1)

		if int(id) in self.contacts:
			user = User (self.user.id)
			contact = User (id)

			# make sure both contacts are in the same conference
			if user.joined_conference.hget() == contact.joined_conference.hget():
				# inject the sending user
				event["user"] = self.user.id
				return event

		return False