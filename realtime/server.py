import os

from autobahn.wamp import WampServerProtocol

# set up django project environment
os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "config.bootstrap")

from django.conf import settings
from django.http import parse_cookie
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import SESSION_KEY, BACKEND_SESSION_KEY, load_backend

from services.user import UserService
from services.chat import ChatService
from services.conference import ConferenceService
from services.whiteboard import WhiteboardService
from services.rtc import RTCService


class WampServer (WampServerProtocol):

	def get_user (self):
		'''
		Get a user from the supplied session within the HTTP cookie
		'''
		from django.contrib.auth.models import AnonymousUser
		try:
			cookie = parse_cookie (self.http_headers['cookie'].encode('utf-8'))
			session_id = cookie[settings.SESSION_COOKIE_NAME]

			session = SessionStore (session_key=session_id)
			backend = load_backend (session[BACKEND_SESSION_KEY])

			user = backend.get_user (session[SESSION_KEY]) or AnonymousUser()
		except KeyError:
			user = AnonymousUser()
		return user


	def onSessionOpen (self):
		user = self.get_user()

		self.user_service = UserService (user)
		self.chat_service = ChatService (user)
		self.conference_service = ConferenceService (self, user)
		self.whiteboard_service = WhiteboardService (user)
		self.rtc_service = RTCService (user)

		self.registerForRpc (self.user_service, "http://coachme.com.au/user#")

		self.registerHandlerForPubSub (self.chat_service, "http://coachme.com.au/")
		
		self.registerForRpc (self.conference_service, "http://coachme.com.au/conference#")
		self.registerHandlerForPubSub (self.conference_service, "http://coachme.com.au/conference/")

		self.registerHandlerForPubSub (self.whiteboard_service, "http://coachme.com.au/")

		self.registerHandlerForPubSub (self.rtc_service, "http://coachme.com.au/")