# You can run this .tac file directly with:
#    twistd -ny service.tac


# set up django project environment
import os, sys

# add the backend path and assign the settings module
os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "config.bootstrap")


from twisted.python import log
from twisted.application import service, internet

from twisted.internet import reactor
from autobahn.websocket import listenWS
from autobahn.wamp import WampServerFactory


# get django app path so we can use the orm
root_path = os.path.abspath (os.path.dirname(__file__))
app_path = os.path.join (os.path.dirname(root_path), "app")

sys.path.append (app_path)
sys.path.append (os.path.join (app_path, "apps"))

import settings, server



class WSServer (internet.TCPServer):

	def __init__ (self, *args, **kwargs):
		uri = "ws://{0}:{1}".format (settings.HOST, settings.PORT)

		self.factory = WampServerFactory (uri, debug=settings.DEBUG)
		self.factory.protocol = server.WampServer


	def privilegedStartService (self):
		pass

	def startService (self):
		listenWS (self.factory, interface=settings.HOST)




application = service.Application ("Realtime Wamp Server")

service = WSServer()
service.setServiceParent (application)