from django.conf.urls import patterns, include, url

from core.api.urls import core_api
from boardroom.api.urls import boardroom_api
from coaches.api.urls import coaches_api
from messagebox.api.urls import messagebox_api
from jobs.api.urls import jobs_api
from events.api.urls import events_api

urlpatterns = patterns('',
	(r'', include(core_api.urls)),
	(r'', include(boardroom_api.urls)),
	(r'', include(coaches_api.urls)),
	(r'', include(messagebox_api.urls)),
	(r'', include(jobs_api.urls)),
	(r'', include(events_api.urls)),
)