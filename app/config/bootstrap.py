# Import the rest of the settings in this package
from applications import *
from auth import *
from cache import *
from databases import *
from logging import *
from paths import *
from search import *
from system import *

try:
	from production import *
except ImportError:
	try:
		from local import *
	except ImportError:
		print 'No production or local settings found. Assuming default values'