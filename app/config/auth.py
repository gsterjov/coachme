AUTHENTICATION_BACKENDS = (
	"django.contrib.auth.backends.ModelBackend",
	"guardian.backends.ObjectPermissionBackend",
)

AUTH_USER_MODEL = "core.User"

ANONYMOUS_USER_ID = -1