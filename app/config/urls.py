from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^grappelli/', include('grappelli.urls')),

	url(r'^api/', include('config.api')),

	url(r'^coach/', include('coaches.urls')),
	url(r'^client/', include('clients.urls')),
	url(r'^messages/', include('messagebox.urls')),
	url(r'^feeds/', include('feeder.urls')),
	url(r'^resources/', include('resources.urls')),
	url(r'^calendar/', include('events.urls')),
	url(r'^boardroom/', include('boardroom.urls')),
	url(r'^', include('core.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()