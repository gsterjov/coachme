# The cache backend to use. By default it will use the dummy cache
# and needs to be configured for local/production systems if the
# cache features are to be used.
CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
	}
}
