#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
	app_path = os.path.abspath (os.path.dirname(__file__))
	root_path = os.path.dirname (app_path)

	sys.path.append (app_path)
	sys.path.append (os.path.join (root_path, "libs"))
	sys.path.append (os.path.join (app_path, "apps"))

	os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "config.bootstrap")

	from django.core.management import execute_from_command_line

	execute_from_command_line (sys.argv)