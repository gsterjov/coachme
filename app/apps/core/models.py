from django.db import models
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.contrib.contenttypes import generic
from django.core.cache import cache

from django.dispatch import receiver
from django.db.models.signals import post_save

from signals import profile_associated


class UserManager (BaseUserManager):
	'''
	A custom user manager to use with django.auth
	'''
	def create_user (self, email, password=None):
		'''
		Create the user account with the email as the unique identifier
		'''
		now = timezone.now()
		if not email:
			raise ValueError ("Users must have an email address")

		user = self.model (
			email = UserManager.normalize_email (email),
			is_staff = False,
			is_active = True,
			is_superuser = False,
			last_login = now,
			date_joined = now
		)
		user.set_password (password)
		user.save (using=self._db)
		return user


	def create_superuser (self, email, password):
		'''
		Same as create user except the user is an admin
		'''
		user = self.create_user (email, password)
		user.is_staff = True
		user.is_superuser = True
		user.save (using=self._db)
		return user


class User (AbstractBaseUser, PermissionsMixin):
	'''
	A custom user model that uses an email address as the unique identifier
	'''
	objects = UserManager()

	email = models.EmailField (max_length=255, unique=True, db_index=True, blank=False)
	first_name = models.CharField(max_length=100, blank=False)
	last_name = models.CharField(max_length=100, blank=True)
	is_staff = models.BooleanField (default=False, help_text="Designates whether the user can log into this admin site")
	is_active = models.BooleanField (default=True, help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts")
	date_joined = models.DateTimeField (default=timezone.now)

	USERNAME_FIELD = "email"
	
	content_type	= models.ForeignKey (generic.ContentType, null=True)
	object_id		= models.PositiveIntegerField (null=True)
	profile			= generic.GenericForeignKey ('content_type', 'object_id')


	@property
	def is_coach (self):
		return self.content_type.name == "coach"

	@property
	def is_client (self):
		return self.content_type.name == "client"

	@property
	def full_name (self):
		return "{0} {1}".format (self.first_name, self.last_name)

	@property
	def contacts (self):
		contacts = [rel.from_user for rel in self.relationships_from.filter(status=Relationship.ACCEPTED).all()]
		contacts += [rel.to_user for rel in self.relationships_to.filter(status=Relationship.ACCEPTED).all()]
		return contacts


	def get_short_name (self):
		return self.first_name

	def is_online (self):
		key = "user_{0}".format (self.id)
		user_cache = cache.get (key)
		return user_cache['online'] if user_cache else False

	def get_relationship (self, user_id):
		'''
		Attempts to get the relationship with the specified user
		if it exists, otherwise return None.
		'''
		relationship = Relationship.objects.filter(
			models.Q(to_user=user_id, from_user=self) |
			models.Q(from_user=user_id, to_user=self)
		)

		if len(relationship) > 0:
			return relationship[0]

		return None


	def __unicode__ (self):
		return self.email



class Relationship (models.Model):
	'''
	A relationship between two users
	'''
	PENDING = "PENDING"
	ACCEPTED = "ACCEPTED"
	DECLINED = "DECLINED"
	STATUS_TYPES = (
		(PENDING, "Pending"),
		(ACCEPTED, "Accepted"),
		(DECLINED, "Declined"),
	)

	from_user = models.ForeignKey (User, related_name="relationships_to")
	to_user = models.ForeignKey (User, related_name="relationships_from")
	status = models.CharField (max_length=255, choices=STATUS_TYPES)
	created_at = models.DateTimeField (auto_now_add=True, default=timezone.now)


	def __str__ (self):
		return "{0} related to {1} [{2}]".format (self.from_user, self.to_user, self.status)




@receiver (post_save, sender=User)
def on_profile_associated (sender, instance=None, created=False, **kwargs):
	if instance.profile:
		profile_associated.send (User, user=instance, profile=instance.profile)