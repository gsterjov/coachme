from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import redirect
from django.contrib import auth
from django.contrib.auth.decorators import login_required

from utils import TemplateResponse
from forms import LoginForm

from webauth.models import Client, Account, Token
from webauth.providers import GoogleProvider


@ensure_csrf_cookie
def home (request):
	return TemplateResponse (request, "core/index.html")


def login (request):
	# log the user in
	if request.method == "POST":
		form = LoginForm (request.POST)
		
		if form.is_valid():
			email		= form.cleaned_data["email"]
			password	= form.cleaned_data["password"]
			
			# try authenticating the user
			user = auth.authenticate (username=email, password=password)
			
			if user is not None:
				if user.is_active:
					auth.login (request, user)
					return redirect ("core.views.home")
		
	# create a new login form
	else:
		form = LoginForm()
	
	# display the login page
	return TemplateResponse (request, "core/login.html", {"form": form})


def logout (request):
	auth.logout (request)
	return redirect ("core.views.home")



@login_required
def connect (request, provider):
	accounts = request.user.web_accounts.filter(client__provider = provider).all()

	# already connected
	if len(accounts) > 0:
		return HttpResponse()


	client = Client.objects.get (provider = provider)

	prov = None
	scope = []

	if provider == "google":
		scope = [
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		]
		prov = GoogleProvider (client, "https://localhost:8000/accounts/google/login/callback/")


	url, state = prov.authorisation_url (scope)
	return HttpResponseRedirect (location=url)