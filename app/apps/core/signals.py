import django.dispatch

profile_associated = django.dispatch.Signal (providing_args=["user", "profile"])