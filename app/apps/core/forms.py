from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from core.models import User


class LoginForm (forms.Form):
	email = forms.EmailField()
	password = forms.CharField (max_length=20, widget=forms.PasswordInput())


class ContactForm (forms.Form):
	email = forms.EmailField()



class CustomUserCreationForm (UserCreationForm):
	'''
	The user create form in the admin panel using the custom
	user without a username field.
	'''
	def __init__ (self, *args, **kwargs):
		super(CustomUserCreationForm, self).__init__ (*args, **kwargs)
		del self.fields["username"]

	class Meta:
		model = User
		fields = ("email",)


class CustomUserChangeForm (UserChangeForm):
	'''
	The user change form in the admin panel using the custom
	user without a username field.
	'''
	def __init__ (self, *args, **kwargs):
		super(CustomUserChangeForm, self).__init__ (*args, **kwargs)
		del self.fields["username"]

	class Meta:
		model = User