import os, uuid, datetime
from functools import wraps

from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth import authenticate, login

from serializer import JSONSerializer

from django.conf import settings


class TemplateResponse (HttpResponse):
	
	def __init__ (self, request, template, dictionary=None):
		
		data = loader.render_to_string (template, dictionary, RequestContext(request))
		
		# create the response
		super(TemplateResponse, self).__init__ (data)


class JsonResponse (HttpResponse):
	
	def __init__ (self, object, fields=None):
		'''
		Serialize an object into JSON. This also works on Django querysets and models.
		Extra options can be provided to alter the JSON output.

		fields: Restrict the output to the provided fields. None will include all fields.
		'''
		serializer = JSONSerializer()
		data = serializer.serialize (object, use_natural_keys=True, fields=fields)
		
		# create the response
		super(JsonResponse, self).__init__ (data, content_type='application/json')


def render (template, context):
	return loader.render_to_string(template, context)


def generate_media_path (path, filename):
	name, ext = os.path.splitext (filename)
	directory = datetime.date.today().strftime (path)
	
	generate_filename = True
	
	while generate_filename:
		filename = "{0}{1}".format (uuid.uuid4(), ext)
		path = os.path.join (directory, filename)
		generate_filename = os.path.exists (os.path.join (settings.MEDIA_ROOT, path))
	
	return path




def http_basic_auth (func):
	'''
	A decorator for HTTP Basic Auth challenges.

	If the user is not logged in or provides the wrong login details,
	a 401 response will be returned.
	'''
	@wraps(func)
	def _decorator (request, *args, **kwargs):
		# already logged in
		if request.user.is_authenticated():
			return func (request, *args, **kwargs)

		# not logged in, process HTTP auth headers
		if request.META.has_key ("HTTP_AUTHORIZATION"):
			method, auth = request.META["HTTP_AUTHORIZATION"].split()

			# got basic auth
			if (method.lower() == "basic"):
				auth = auth.strip().decode ("base64")
				username, password = auth.split (":", 1)
				user = authenticate (username=username, password=password)

				# authentication success. login and run the view
				if user is not None and user.is_active:
					login (request, user)
					request.user = user
					return func (request, *args, **kwargs)

		# authentication failed
		response = HttpResponse()
		response.status_code = 401
		response["WWW-Authenticate"] = 'Basic realm="coachme.com.au"'
		return response

	return _decorator