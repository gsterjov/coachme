from django.forms import ModelChoiceField

from tastypie.validation import Validation, CleanedDataFormValidation

from core.forms import ContactForm
from core.models import User, Relationship


class FormValidation (CleanedDataFormValidation):
	"""
	A form validation that works with relationships by converting resource URIs to PKs.
	"""

	def uri_to_pk (self, uri):
		"""
		Returns the pk, or list of pks, within the resource_uri, or list of resource_uris.
		This will only work if the uri ends with the pk, ie. ``/api/v1/resource/123/``.
		"""
		if uri is None:
			return None

		#convert everything to lists
		multiple = not isinstance(uri, basestring)
		uris = uri if multiple else [uri]

		#handle all passed URIs
		converted = []
		for one_uri in uris:
			try:
				converted.append (int(one_uri.split("/")[-2]))
			except (IndexError, ValueError):
				raise ValueError ("URI %s could not be converted to PK integer" % one_uri)

		# convert back to original format
		return converted if multiple else converted[0]


	def form_args (self, bundle):
		kwargs = super(FormValidation, self).form_args (bundle)

		relation_fields = [name for name, field in self.form_class.base_fields.items() if issubclass(field.__class__, ModelChoiceField)]

		for field in relation_fields:
			if field in kwargs["data"]:
				kwargs["data"][field] = self.uri_to_pk (kwargs["data"][field])

		return kwargs




class ContactValidation (Validation):
	'''
	Custom validation to make sure relationships are unique
	'''
	def is_valid (self, bundle, request=None):
		errors = {}

		try:
			form = ContactForm (bundle.data)

			if form.is_valid():
				bundle.data["email"] = form.cleaned_data["email"]
				contact = User.objects.get (email=bundle.data["email"])

				# current user has already added the contact
				if Relationship.objects.filter(from_user=bundle.request.user, to_user=contact).exists():
					errors["email"] = ["The specified email already belongs to a contact on your list"]

				# the contact has already added the current user
				elif Relationship.objects.filter(from_user=contact, to_user=bundle.request.user).exists():
					errors["email"] = ["The specified email already belongs to a contact on your list"]
			else:
				errors = form.errors

		except User.DoesNotExist:
			errors["email"] = ["The specified email does not exist"]

		return errors