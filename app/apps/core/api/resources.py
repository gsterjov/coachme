from django.conf import settings
from django.conf.urls import url
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.core.paginator import Paginator, InvalidPage
from django.http import HttpResponseNotFound

from tastypie import fields
from tastypie.utils import trailing_slash
from tastypie.resources import Resource, ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import ImmediateHttpResponse
from tastypie.http import HttpAccepted, HttpCreated, HttpUnauthorized, HttpForbidden

from haystack.query import SearchQuerySet
from sorl.thumbnail import get_thumbnail

from webauth.models import Client, Account, Token
from webauth.providers import GoogleProvider

from validation import ContactValidation
from core.models import User, Relationship



class MultipartResource (object):
	'''
	Multipart mixin to allow for ajax file uploads through the API.

	File uploads have to be sent as a PUT request so that django can still process
	it and store it in FILES, but it will still be treated as a PATCH request by the API.
	'''
	def deserialize (self, request, data, format=None):
		if not format:
			format = request.META.get ("CONTENT_TYPE", "application/json")

		if format == "application/x-www-form-urlencoded":
			return request.POST

		if format.startswith ("multipart"):
			data = request.POST.copy()
			data.update (request.FILES)
			return data

		return super(MultipartResource, self).deserialize (request, data, format)

	# Django wont process file uploads unless its a POST or PUT request,
	# so we treat multipart PUT requests as a patch as a work around
	def put_detail (self, request, **kwargs):
		if request.META.get("CONTENT_TYPE").startswith("multipart") and not hasattr(request, "_body"):
			request._body = "";

		if request.META.get("CONTENT_TYPE").startswith("multipart"):
			return super(MultipartResource, self).patch_detail (request, **kwargs)
		else:
			return super(MultipartResource, self).put_detail (request, **kwargs)

	def patch_detail (self, request, **kwargs):
		if request.META.get("CONTENT_TYPE").startswith("multipart") and not hasattr(request, "_body"):
			request._body = "";

		return super(MultipartResource, self).patch_detail (request, **kwargs)



class SearchResource (ModelResource):
	'''
	Search mixin which adds search support to resource through haystack.

	This will added a /search to the possible resource url and will accept the
	?q= keyword query parameter. It also paginates the results with the use of
	the ?page= parameter.
	'''
	def prepend_urls (self):
		return [
			url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view("get_search")),
		]

	def get_search (self, request, **kwargs):
		self.method_check (request, allowed=["get"])
		self.is_authenticated (request)
		self.throttle_check (request)

		query = SearchQuerySet().models(self._meta.queryset.model).load_all().auto_query (request.GET.get("q", ""))
		paginator = Paginator (query, 20)

		try:
			page = paginator.page (int(request.GET.get("page", 1)))
		except InvalidPage:
			raise HttpResponseNotFound ("Sorry, no results on that page.")

		objects = []

		for result in page.object_list:
			if result:
				bundle = self.build_bundle (obj=result.object, request=request)
				bundle = self.full_dehydrate (bundle)
				objects.append (bundle)

		object_list = {
			"objects": objects,
		}

		self.log_throttled_access (request)
		return self.create_response (request, object_list)



class UserResource (ModelResource):
	'''
	The user resource
	'''
	id = fields.CharField (attribute='id')
	first_name = fields.CharField (attribute="first_name")
	last_name = fields.CharField (attribute="last_name")
	full_name = fields.CharField (attribute="full_name")
	photo_small = fields.FileField()
	photo_medium = fields.FileField()
	photo_large = fields.FileField()

	is_coach = fields.BooleanField (attribute="is_coach", readonly=True)
	is_client = fields.BooleanField (attribute="is_client", readonly=True)


	class Meta:
		queryset = User.objects.all()
		fields = ["id", "first_name", "last_name", "full_name", "photo_small", "photo_medium", "photo_large"]
		allowed_methods = ["get"]
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()


	def dispatch (self, request_type, request, **kwargs):
		if kwargs.has_key("pk") and kwargs["pk"] == "me":
			kwargs["pk"] = request.user.id

		return super(UserResource, self).dispatch (request_type, request, **kwargs)


	def prepend_urls (self):
		return [
			url(r"^(?P<resource_name>%s)/login%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view("login"), name="api_login"),
		]


	def login (self, request, **kwargs):
		self.method_check (request, allowed=["post"])

		data = self.deserialize (request, request.body, format=request.META.get("CONTENT_TYPE", "application/json"))

		email = data.get("email", "")
		password = data.get("password", "")
		user = authenticate (username=email, password=password)

		if user is not None:
			if user.is_active:
				login (request, user)
				return HttpAccepted()
			else:
				return HttpForbidden()

		return HttpUnauthorized()


	def generate_thumbnail (self, bundle, size):
		photo = None
		user = bundle.request.user

		if user.is_coach and user.profile.photo:
			photo = get_thumbnail (user.profile.photo, size, crop="center", quality=99).url

		return photo


	def dehydrate_photo_small (self, bundle):
		return self.generate_thumbnail (bundle, "35x35")

	def dehydrate_photo_medium (self, bundle):
		return self.generate_thumbnail (bundle, "100x100")

	def dehydrate_photo_large (self, bundle):
		return self.generate_thumbnail (bundle, "200x150")



class RelationshipResource (ModelResource):
	'''
	The relationship resource
	Creates and modifies relationship requests
	'''
	id = fields.CharField (attribute='id')
	status = fields.CharField (attribute="status")

	# Relations
	from_user = fields.ToOneField (UserResource, "from_user", full=True)
	to_user = fields.ToOneField (UserResource, "to_user", full=True)

	class Meta:
		queryset = Relationship.objects.all()
		fields = ["id", "from_user", "to_user", "status"]

		list_allowed_methods = ["get"]
		detail_allowed_methods = ["get"]

		filtering = {
			"status": ('exact',),
			"from_user": ('exact',),
			"to_user": ('exact',),
		}

		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()

	def authorized_read_list (self, objects, bundle):
		return objects.filter(from_user=bundle.request.user) | objects.filter(to_user=bundle.request.user)



class ContactResource (Resource):
	'''
	The contact resource
	An abstracted view of user relationships. Effectively a list of all added users and requests
	'''
	id = fields.CharField()
	name = fields.CharField()
	online = fields.BooleanField()
	status = fields.CharField()
	added = fields.CharField()

	class Meta:
		include_resource_uri = False
		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["put"]

		filtering = {
			"status": ('exact',),
		}

		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()
		validation = ContactValidation()


	def hydrate (self, bundle):
		bundle.obj.from_user = bundle.request.user
		bundle.obj.status = Relationship.PENDING
		return bundle


	def dehydrate (self, bundle):
		user = bundle.obj["user"]

		bundle.data["id"] = user.id
		bundle.data["name"] = user.full_name
		bundle.data["online"] = user.is_online()
		bundle.data["status"] = bundle.obj["status"]
		bundle.data["added"] = bundle.obj["added"]

		return bundle.data


	def build_filters (self, filters=None):
		applicable_filters = {}

		if filters.has_key ("status"):
			applicable_filters["status"] = filters["status"]

		return applicable_filters


	def obj_create (self, bundle):
		bundle.obj = Relationship()
		bundle = self.full_hydrate (bundle)

		self.is_valid (bundle)

		if bundle.errors:
			raise ImmediateHttpResponse (response=self.error_response (bundle.request, bundle.errors))

		# get the added user *after* validation
		bundle.obj.to_user = User.objects.get (email=bundle.data["email"])
		bundle.obj.save()

		return bundle


	def obj_update (self, bundle, **kwargs):
		relationship = Relationship.objects.get (from_user=bundle.data["id"], to_user=bundle.request.user)
		relationship.status = bundle.data["status"]
		relationship.save()
		
		return bundle


	def obj_get_list (self, bundle):
		user = bundle.request.user
		contacts = []
		filters = {}

		# filters
		if hasattr (bundle.request, "GET"):
			filters = bundle.request.GET.copy()

		applicable_filters = self.build_filters (filters)


		# get relationships where the current user added someone
		relationships = Relationship.objects.filter (from_user=user, **applicable_filters)
		for rel in relationships:
			contacts.append ({
				"user": rel.to_user,
				"status": rel.status,
				"added": True,
			})

		# get relationships where someone added the current user
		relationships = Relationship.objects.filter (to_user=user, **applicable_filters)
		for rel in relationships:
			contacts.append ({
				"user": rel.from_user,
				"status": rel.status,
				"added": False,
			})

		return contacts