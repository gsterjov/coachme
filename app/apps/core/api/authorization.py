from tastypie.authorization import DjangoAuthorization
from tastypie.exceptions import Unauthorized


class GuardianAuthorization (DjangoAuthorization):
	"""

	GuardianAuthorization

		Object level permission checking with django-guardian for django models exposed via tastypie.

	:param model_class: The model class to use when building the permission name. ie add_user
	:type model_class: django.model.Model

	:param view: will permission checks be carried out for view operations?
	:type view: boolean
	:param view_permission: the permission name that signifies the user can view the detail
	:type view_permission: string

	:param add: will permission checks be carried out for add operations?
	:type add: boolean
	:param add_permission: the permission name that signifies the user can add one of these objects
	:type add_permission: string

	:param change: will permission checks be carried out for change operations?
	:type change: boolean
	:param change_permission: the permission name that signifies the user can change one of these objects
	:type change_permission: string

	:param delete: will permission checks be carried out for delete operations?
	:type delete: boolean
	:param delete_permission: the permission code that signifies the user can delete one of these objects
	:type delete_permission: string

	:return values:
		Empty list : When user requests a list of resources for which they have no
					 permissions for any of the items
		HttpForbidden : When user does not have nessecary permissions for an item
		HttpApplicationError : When resource being requested isn't a valid django model.



		class Something(models.Model):
			name = models.CharField()

		class SomethingResource(ModelResource):
			class Meta:
				queryset = Something.objects.all()
				authorization = GuardianAuthorization(model_class=Something)

	"""

	def __init__ (self, *args, **kwargs):
		model_class = kwargs.pop("model_class")
		perm_name = model_class.__name__.lower()

		self.requires_view = kwargs.pop ("view", True)
		self.view_permission = kwargs.pop ("view_permission", "view_{0}".format(perm_name))

		self.requires_create = kwargs.pop ("add", True)
		self.create_permission = kwargs.pop ("add_permission", "add_{0}".format(perm_name))

		self.requires_change = kwargs.pop ("change", True)
		self.change_permission = kwargs.pop ("change_permission", "change_{0}".format(perm_name))

		self.requires_delete = kwargs.pop ("delete", True)
		self.delete_permission = kwargs.pop ("delete_permission", "delete_{0}".format(perm_name))


	def is_site_moderator (self, user=None):
		if user.is_superuser:
			return True
		return False


	def requires_check (self, permission):
		if permission is self.create_permission and not self.requires_create:
			return False
		elif permission is self.view_permission and not self.requires_view:
			return False
		elif permission is self.change_permission and not self.requires_change:
			return False
		elif permission is self.delete_permission and not self.requires_delete:
			return False
		return True


	def generic_base_check (self, object_list, bundle):
		"""
			Raises a HttpApplicationError exception if either:
				a) if the `object_list.model` doesn't have a `_meta` attribute
				b) the `bundle.request` object doesn't have a `user` attribute
		"""
		if not self.base_checks (bundle.request, object_list.model):
			return HttpApplicationError("Invalid resource.")
		return True


	def generic_item_check (self, object_list, bundle, permission):
		"""
			Single item check, returns boolean indicating that the user
			can access the item resource.
		"""
		user = bundle.request.user

		if not self.generic_base_check (object_list, bundle):
			raise Unauthorized ("You are not allowed to access that resource.")

		if self.is_site_moderator (user) or not self.requires_check (permission):
			return True

		if not user.has_perm (permission, bundle.obj):
			raise Unauthorized ("You are not allowed to access that resource.")

		return True


	def generic_list_check (self, object_list, bundle, permission):
		"""
			Multiple item check, returns queryset of resource items the user
			can access.
		"""
		user = bundle.request.user

		if not self.generic_base_check (object_list, bundle):
			raise Unauthorized ("You are not allowed to access that resource.")

		if self.is_site_moderator (user) or not self.requires_check (permission):
			return object_list

		return [i for i in object_list if user.has_perm (permission, i)]


	def generic_post_check (self, object_list, bundle, permission):
		user = bundle.request.user

		if not self.generic_base_check (object_list, bundle):
			raise Unauthorized ("You are not allowed to access that resource.")

		if self.is_site_moderator (user) or not self.requires_check (permission):
			return True

		if not user.has_perm (permission):
			raise Unauthorized ("You are not allowed to access that resource.")

		return True


	# List Checks
	def create_list (self, object_list, bundle):
		return self.generic_list_check (object_list, bundle, self.create_permission)

	def read_list (self, object_list, bundle):
		return self.generic_list_check (object_list, bundle, self.view_permission)

	def update_list (self, object_list, bundle):
		return self.generic_list_check (object_list, bundle, self.change_permission)

	def delete_list (self, object_list, bundle):
		return self.generic_list_check (object_list, bundle, self.delete_permission)

	# Item Checks
	def create_detail (self, object_list, bundle):
		return self.generic_post_check (object_list, bundle, self.create_permission)

	def read_detail (self, object_list, bundle):
		return self.generic_item_check (object_list, bundle, self.view_permission)

	def update_detail (self, object_list, bundle):
		return self.generic_item_check (object_list, bundle, self.change_permission)

	def delete_detail (self, object_list, bundle):
		return self.generic_item_check (object_list, bundle, self.delete_permission)