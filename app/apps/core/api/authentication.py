from django.conf import settings
from django.utils.http import same_origin
from django.middleware.csrf import _sanitize_token, constant_time_compare

from tastypie.authentication import Authentication


class BypassAuthentication (Authentication):
	'''
	Bypasses authentication for specific methods.

	This is almost identical with SessionAuthentication except that
	it checks the supplied bypass methods rather than the user session.
	'''
	def __init__ (self, methods=[]):
		self.methods = methods


	def is_authenticated (self, request, **kwargs):
		if request.method in ("GET", "HEAD", "OPTIONS", "TRACE"):
			return request.method.lower() in self.methods

		if getattr(request, '_dont_enforce_csrf_checks', False):
			return request.method.lower() in self.methods

		csrf_token = _sanitize_token (request.COOKIES.get (settings.CSRF_COOKIE_NAME, ''))

		if request.is_secure():
			referer = request.META.get ("HTTP_REFERER")

			if referer is None:
				return False

			good_referer = "https://%s/" % request.get_host()

			if not same_origin (referer, good_referer):
				return False

		request_csrf_token = request.META.get ("HTTP_X_CSRFTOKEN", "")

		if not constant_time_compare (request_csrf_token, csrf_token):
			return False

		return request.method.lower() in self.methods


	def get_identifier (self, request):
		return "Anonymous"