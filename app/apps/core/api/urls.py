from django.conf.urls import patterns, include, url
from tastypie.api import Api

from resources import UserResource, RelationshipResource, ContactResource


core_api = Api (api_name='core')

core_api.register (UserResource())
core_api.register (RelationshipResource())
core_api.register (ContactResource())