from django.forms.widgets import DateInput, FileInput, SelectMultiple
from django.utils.safestring import mark_safe
from django.utils import dateformat, formats


class DatePicker (DateInput):
	'''
	A bootstrap based date picker.
	'''
	def __init__ (self, attrs={}):
		date_format = formats.get_format("SHORT_DATE_FORMAT")

		attrs["class"] = "datepicker"
		attrs["data-provide"] = "datepicker"
		attrs["data-date-format"] = self.js_date_format (date_format)
		
		super(DatePicker, self).__init__ (attrs)


	template = """
		<div class="input-append">
			%(input)s
			<button class="btn" data-provide="utils-clear-field" data-target="#%(clear_target)s"><i class="icon-remove"></i></button>
		</div>
	"""


	def js_date_format (self, format):
		format = format.replace ("%", "")
		format = format.replace ("Y", "yyyy")
		format = format.replace ("m", "mm")
		format = format.replace ("d", "dd")

		return format


	def render (self, name, value, attrs=None):
		if value:
			value = dateformat.format(value, formats.get_format('SHORT_DATE_FORMAT', use_l10n=True))

		widget_input = super(DatePicker, self).render (name, value, attrs)

		substitutions = {
			'input': widget_input,
			'clear_target': attrs["id"],
		}

		return mark_safe (self.template % substitutions)


class BootstrapFileUpload (FileInput):
	'''
	A file upload widget using the bootstrap-fileupload plugin.
	'''
	def __init__ (self, attrs=None):
		super(BootstrapFileUpload, self).__init__ (attrs)


	template = """
		<div class="fileupload fileupload-%(state)s" data-provides="fileupload">
			<div class="input-append">
				<div class="uneditable-input span3">
					<i class="icon-file fileupload-exists"></i>
					<span class="fileupload-preview"></span>
				</div>
				<span class="btn btn-file">
					<span class="fileupload-new">Select file</span>
					<span class="fileupload-exists">Change</span>
					%(input)s
				</span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
			</div>
		</div>
	"""


	def render (self, name, value, attrs=None):
		widget_input = super(BootstrapFileUpload, self).render (name, value, attrs)

		substitutions = {
			'state': 'exists' if value else 'new',
			'input': widget_input,
			'name': name,
		}

		return mark_safe (self.template % substitutions)


class BootstrapImageUpload (FileInput):
	'''
	An image upload widget using the bootstrap-fileupload plugin.
	'''
	def __init__ (self, attrs=None, placeholder_url=None):
		super(BootstrapImageUpload, self).__init__ (attrs)

		self.placeholder_url = placeholder_url if placeholder_url else ''
		self.max_width = getattr(attrs, 'max-width', 200)
		self.max_height = getattr(attrs, 'max-height', 150)


	template = """
		<div class="fileupload fileupload-%(state)s" data-provides="fileupload">
			<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="%(placeholder_url)s" /></div>
			<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: %(max_width)dpx; max-height: %(max_height)dpx; line-height: 20px"><img src="%(url)s" /></div>
			<div>
				<span class="btn btn-file">
					<span class="fileupload-new">Select image</span>
					<span class="fileupload-exists">Change</span>
					%(input)s
				</span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
			</div>
		</div>
	"""


	def render (self, name, value, attrs=None):
		widget_input = super(BootstrapImageUpload, self).render (name, value, attrs)

		max_width = getattr(attrs, 'max-width', self.max_width)
		max_height = getattr(attrs, 'max-height', self.max_height)

		substitutions = {
			'state': 'exists' if value else 'new',
			'placeholder_url': self.placeholder_url,
			'url': value.url if value else '',
			'max_width': max_width,
			'max_height': max_height,
			'input': widget_input,
			'name': name,
		}

		return mark_safe (self.template % substitutions)



class ChosenSelectMultiple (SelectMultiple):
	'''
	A multiple select drop down using Chosen
	'''
	def __init__ (self, attrs={}, placeholder=None):
		attrs["data-provide"] = "chosen"
		attrs["data-placeholder"] = placeholder
		super(ChosenSelectMultiple, self).__init__ (attrs)


	def render (self, *args, **kwargs):
		self.attrs.update ({"data-optional": self.is_required})
		return super(ChosenSelectMultiple, self).render (*args, **kwargs)