from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from core.utils import TemplateResponse

from models import Feed
from forms import AddForm


@login_required
def add (request):
	# add the new feed
	if request.method == 'POST':
		form = AddForm (request.POST)
		
		if form.is_valid():
			profile = request.user.profile

			feed = Feed()
			feed.coach = profile
			feed.url = form.cleaned_data['url']
			feed.save()

			return redirect("coaches.views.profile", profile.id)

	else:
		form = AddForm()

	return TemplateResponse (request, "feeder/add.html", {"form": form})