from django.db import models
from django.utils.timezone import now

from coaches.models import Coach


class Feed (models.Model):
	coach	= models.ForeignKey (Coach)
	url		= models.CharField (max_length=100)

	added_at = models.DateTimeField (default=now)
	updated_at = models.DateTimeField (null=True)