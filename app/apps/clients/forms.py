from django import forms

class CreateForm (forms.Form):
	full_name	= forms.CharField (max_length=255, error_messages={'required': 'Please enter your full name'})
	password	= forms.CharField (max_length=20, error_messages={'required': 'Please enter a password'}, widget=forms.PasswordInput())
	
	email = forms.EmailField (error_messages={
		'required': 'Please enter your email address',
		'invalid': 'An email address must be of the form "example@email.com"'
	})