from django.contrib.auth import get_user_model

from core.utils import TemplateResponse

from forms import CreateForm
from models import Client


def create (request):
	# register the new user
	if request.method == 'POST':
		form = CreateForm (request.POST)
		
		if form.is_valid():

			# create client profile
			client = Client()
			client.save()
			
			# create user
			email		= form.cleaned_data['email']
			password	= form.cleaned_data['password']
			
			User = get_user_model()
			user = User.objects.create_user (email, password)
			user.profile	= client
			user.full_name	= form.cleaned_data['full_name']
			
			user.save()
			return TemplateResponse (request, "clients/create_finished")

	else:
		form = CreateForm()

	return TemplateResponse (request, "clients/create.html", {"form": form})