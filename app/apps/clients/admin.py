from django.contrib import admin
from clients.models import Client


class ClientAdmin (admin.ModelAdmin):
	list_display = ("full_name", "email")

	def full_name (self, obj):
		return obj.user.full_name

	def email (self, obj):
		return obj.user.email


admin.site.register (Client, ClientAdmin)