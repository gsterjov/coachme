from django.conf import settings
from django.db import models
from django.contrib.contenttypes import generic
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from django.dispatch import receiver, Signal
from django.db.models.signals import post_save
from guardian.shortcuts import assign_perm

from core.signals import profile_associated


class Client (models.Model):
	auth_user = generic.GenericRelation (settings.AUTH_USER_MODEL)
	
	@property
	def user (self):
		return self.auth_user.get()




@receiver (profile_associated, sender=get_user_model())
def on_profile_associated (sender, user=None, profile=False, **kwargs):
	if user.is_client:
		group = Group.objects.get (name="Clients")
		user.groups.add (group)

		assign_perm ("change_client", user, profile)
		assign_perm ("delete_client", user, profile)