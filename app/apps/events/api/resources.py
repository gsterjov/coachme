from tastypie import fields
from tastypie.resources import Resource, ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization

from events.models import Event


class EventResource (ModelResource):
	'''
	An event resource.
	An event can be associated with pretty much any model.
	'''
	title = fields.CharField()


	class Meta:
		queryset = Event.objects.all()
		fields = ["id", "start", "end"]

		list_allowed_methods = ["get"]
		detail_allowed_methods = ["get"]

		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()


	def authorized_read_list (self, objects, bundle):
		return objects.filter (user = bundle.request.user)


	def dehydrate_title (self, bundle):
		if hasattr(bundle.obj.regarding, "title"):
			return bundle.obj.regarding.title

		return None