from tastypie.api import Api
from resources import EventResource

events_api = Api (api_name='events')
events_api.register (EventResource())