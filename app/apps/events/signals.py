import django.dispatch

delete_event = django.dispatch.Signal (providing_args=["regarding"])
create_event = django.dispatch.Signal (providing_args=["user", "regarding", "start", "end"])