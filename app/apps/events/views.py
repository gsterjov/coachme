import json
import icalendar

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound, HttpResponseServerError

from django.utils.timezone import now
from django.contrib.auth.decorators import login_required

from core.utils import TemplateResponse, http_basic_auth

from webauth.models import Client, Account, Token
from webauth.providers import GoogleProvider

from models import Event, RemoteCalendar, RemoteEvent


@http_basic_auth
def feed (request):
	'''
	The iCalendar feed of all events for the authenticated user.
	'''
	cal = icalendar.Calendar()

	# metadata
	cal.add ("version", "2.0")
	cal.add ("prodid", "-//coachme.com.au//Calendar//EN")
	cal.add ("calscale", "GREGORIAN")
	cal.add ("method", "PUBLISH")
	cal.add ("x-wr-timezone", "UTC")
	cal.add ("x-wr-calname", "Coach Me Calendar")
	cal.add ("x-wr-caldesc", "Your Coach Me boardroom calendar of tasks, milestones and other events")

	# events
	events = request.user.event_set.all().order_by ("-start")
	
	for event in events:
		ev = icalendar.Event()
		ev.add ("uid", "coachme.com.au/calendar/event/{0}".format(event.id))
		ev.add ("summary", event.regarding.title)
		ev.add ("description", event.regarding.description)
		ev.add ("dtstart", event.start)

		if event.end is not None:
			ev.add ("dtend", event.end)

		ev.add ("dtstamp", now())
		ev.add ("created", event.added_at)

		if event.updated_at:
			ev.add ("last-modified", event.updated_at)

		cal.add_component (ev)

	# render the feed
	response = HttpResponse (cal.to_ical(), mimetype="mime_type='text/calendar; charset=utf8'")
	response["Content-Disposition"] = "attachment; filename=calendar.ics"
	return response



@login_required
def connect (request):
	client = Client.objects.get (provider = "google")
	provider = GoogleProvider (client, redirect_uri="https://localhost:8001/calendar/connect/response/")

	scope = ["https://www.googleapis.com/auth/calendar"]

	url, state = provider.authorisation_url (scope)
	request.session["calendar_oauth_state"] = state

	return HttpResponseRedirect (url)


@login_required
def connect_response (request):
	state = request.GET["state"]

	if request.GET.has_key ("error"):
		error = request.GET["error"]

	elif request.GET.has_key ("code"):
		code = request.GET["code"]

		client = Client.objects.get (provider = "google")
		provider = GoogleProvider (
			client,
			redirect_uri="https://localhost:8001/calendar/connect/response/",
			state=request.session["calendar_oauth_state"]
		)

		token = provider.token (state, code)

		# find existing account
		try:
			account = request.user.web_accounts.get (client__provider = "google")
		# create account
		except Account.DoesNotExist:
			account = Account()
			account.user = request.user
			account.client = client
			account.save()

		# find existing calendar token
		try:
			tok = account.token_set.get (name = "calendar")
		# add token
		except Token.DoesNotExist:
			tok = Token()
			tok.account = account
			tok.name = "calendar"

		tok.access = token["access_token"]
		tok.expires_at = token["expires_at"]
		tok.expires_in = token["expires_in"]
		tok.save()


	return TemplateResponse (request, "core/accounts/connected.html")



@login_required
def sync (request):
	try:
		account = request.user.web_accounts.get (client__provider = "google")
		token = account.token_set.get (name = "calendar")

		# get or create a calendar if it doesnt exist
		try:
			calendar = account.remote_calendar_set.get()
			
		except RemoteCalendar.DoesNotExist:
			calendar = RemoteCalendar()
			calendar.account = account
			calendar.sync (token)

		# send all existing events
		for event in request.user.event_set.all():
			try:
				remote_event = event.remote_event_set.get()
			except RemoteEvent.DoesNotExist:
				remote_event = RemoteEvent()
				remote_event.calendar = calendar
				remote_event.event = event
				remote_event.sync (token)


		return HttpResponse (status = 202) # accepted

	except Account.DoesNotExist:
		return HttpResponseNotFound ("Could not find a connected account")

	except Token.DoesNotExist:
		return HttpResponseNotFound ("Could not find an authorised calendar token")

	return HttpResponseServerError()