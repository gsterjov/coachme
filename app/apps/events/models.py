import json

from django.conf import settings
from django.db import models
from django.utils.timezone import now
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType

from django.dispatch import receiver
from signals import create_event, delete_event

from webauth.models import Account
from webauth.providers import GoogleProvider

from utils import rfc3999_format


class Event (models.Model):
	"""
	A generic event assigned to a user that can be attached to any model
	"""
	user			= models.ForeignKey (settings.AUTH_USER_MODEL)

	content_type	= models.ForeignKey (generic.ContentType, null=True)
	object_id		= models.PositiveIntegerField (null=True)
	regarding		= generic.GenericForeignKey ("content_type", "object_id")

	start			= models.DateTimeField()
	end				= models.DateTimeField (null=True)

	added_at		= models.DateTimeField (default=now)
	updated_at		= models.DateTimeField (null=True)




class RemoteCalendar (models.Model):
	"""
	A synchronised calendar resource
	"""
	account = models.ForeignKey (Account, related_name="remote_calendar_set")
	resource_id = models.CharField (max_length=200)


	def sync (self, token):
		"""
		Creates or updates the remote version of the calendar.
		Will call `save()` if the sync was successful
		"""
		if not self.resource_id:
			provider = GoogleProvider (self.account.client)

			url = "https://www.googleapis.com/calendar/v3/calendars"
			headers = {"content-type": "application/json"}

			data = {
				"summary": "CoachMe",
				"description": "Your coachme.com.au events, tasks and milestones",
			}

			resp = provider.post (token, url, json.dumps(data), headers=headers)
			resp.raise_for_status()
			resp = resp.json()

			self.resource_id = resp["id"]
			self.save()



class RemoteEvent (models.Model):
	"""
	An event resource from a synchronised calendar
	"""
	calendar = models.ForeignKey (RemoteCalendar, related_name="remote_event_set")
	resource_id = models.CharField (max_length=200)

	event = models.ForeignKey (Event, related_name="remote_event_set")


	def sync (self, token):
		"""
		Creates or updates the remote version of the calendar event.
		Will call `save()` if the sync was successful
		"""
		if not self.resource_id:
			provider = GoogleProvider (token.account.client)

			url = "https://www.googleapis.com/calendar/v3/calendars/{0}/events".format (self.calendar.resource_id)
			headers = {"content-type": "application/json"}

			data = {
				"summary": self.event.regarding.title,
				"description": self.event.regarding.description,
			}

			if self.event.end is None:
				data["start"] = { "date": self.event.start.strftime("%Y-%m-%d") }
				data["end"] = { "date": self.event.start.strftime("%Y-%m-%d") }
			else:
				data["start"] = { "dateTime": rfc3999_format (self.event.start) }
				data["end"] = { "dateTime": rfc3999_format (self.event.end) }

			resp = provider.post (token, url, json.dumps(data), headers=headers)
			resp.raise_for_status()
			resp = resp.json()

			self.resource_id = resp["id"]
			self.save()




@receiver (create_event)
def on_create_event (sender, user, regarding, start, end, **kwargs):
	content_type = ContentType.objects.get_for_model (regarding)

	try:
		event = Event.objects.get (object_id=regarding.pk, content_type=content_type)

	except Event.DoesNotExist:
		event = Event()
		event.user = user
		event.regarding = regarding

	event.start = start
	event.end = end
	event.save()


@receiver (delete_event)
def on_delete_event (sender, regarding, **kwargs):
	content_type = ContentType.objects.get_for_model (regarding)

	try:
		event = Event.objects.get (object_id=regarding.pk, content_type=content_type)
		event.delete()
	except Event.DoesNotExist:
		pass