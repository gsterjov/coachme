
def rfc3999_format (dt):
	d = dt.strftime ("%Y-%m-%dT%H:%M:%S%z")
	return d[:-2] + ":" + d[-2:]