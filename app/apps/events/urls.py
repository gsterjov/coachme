from django.conf.urls import patterns, include, url


urlpatterns = patterns('events.views',
	url(r'feed$', 'feed'),
	url(r'connect/response/$', 'connect_response'),
	url(r'connect$', 'connect'),
	url(r'sync$', 'sync'),
)