from django.conf import settings
from django.db import models
from django.utils.timezone import now

from core.utils import generate_media_path

# import settings


def get_upload_path (instance, filename):
	return generate_media_path (settings.RESOURCES_FILE_DIR, filename)


class Resource (models.Model):
	user		= models.ForeignKey (settings.AUTH_USER_MODEL)
	title		= models.CharField (max_length=100)
	description	= models.TextField (null=True, max_length=500)
	filename	= models.CharField (max_length=100)
	file		= models.FileField (upload_to=get_upload_path)

	added_at	= models.DateTimeField (default=now)
	updated_at	= models.DateTimeField (null=True)