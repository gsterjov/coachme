from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from core.utils import TemplateResponse

from models import Resource
from forms import AddForm


@login_required
def add (request):
	# add the new resource
	if request.method == 'POST':
		form = AddForm (request.POST, request.FILES)
		
		if form.is_valid():
			profile = request.user.profile

			resource = Resource()
			resource.user = request.user
			resource.title = form.cleaned_data['title']
			resource.description = form.cleaned_data['description']
			resource.filename = form.cleaned_data['resource'].name
			resource.file = form.cleaned_data['resource']
			resource.save()

			return redirect("coaches.views.profile", profile.id)

	else:
		form = AddForm()

	return TemplateResponse (request, "resources/add.html", {"form": form})