from django import forms
from core.widgets import BootstrapFileUpload

class AddForm (forms.Form):
	title		= forms.CharField (max_length=100)
	description	= forms.CharField (max_length=500, required=False, widget=forms.Textarea)
	resource	= forms.FileField (widget=BootstrapFileUpload)