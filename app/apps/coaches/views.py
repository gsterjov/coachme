from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from haystack.query import SearchQuerySet

from core.utils import TemplateResponse

from forms import CoachForm, SearchForm, ProfileForm
from models import Coach


def create (request):
	# register the new user
	if request.method == 'POST':
		form = CreateForm (request.POST)
		
		if form.is_valid():

			# create coach profile
			coach = Coach()
			coach.save()
			
			# create user
			email		= form.cleaned_data['email']
			password	= form.cleaned_data['password']
			
			User = get_user_model()
			user = User.objects.create_user (email, password)
			user.profile	= coach
			user.full_name	= form.cleaned_data['full_name']
			
			user.save()
			return TemplateResponse (request, "coaches/create_finished")

	else:
		form = CreateForm()

	return TemplateResponse (request, "coaches/create.html", {"form": form})


def list (request):
	coaches = Coach.objects.all()
	return TemplateResponse (request, "coaches/list.html", {"coaches": coaches})


def search (request):
	# search for a coach
	if request.GET.has_key ("q"):
		form = SearchForm (request.GET)

		if form.is_valid():
			query = form.cleaned_data["q"]
			
			states		= [state.id for state in form.cleaned_data["states"]]
			skills		= [skill.id for skill in form.cleaned_data["skills"]]
			industries	= [industry.id for industry in form.cleaned_data["industries"]]

			results = SearchQuerySet().models(Coach)
			results = results.filter(states__in=states, skills__in=skills, industries__in=industries).auto_query(query)

			paginator = Paginator (results, 20)

			try:
				page = paginator.page (request.GET.get("page", 1))
			except PageNotAnInteger:
				page = paginator.page (1)
			except EmptyPage:
				page = paginator.page (paginator.num_pages)

			return TemplateResponse (request, "coaches/search.html", {"form": form, "page": page, "query": query})

	else:
		form = SearchForm()
	
	return TemplateResponse (request, "coaches/search.html", {"form": form})


def profile (request, id):
	coach = get_object_or_404 (Coach, pk=id)
	return TemplateResponse (request, "coaches/profile.html", {"coach": coach})


@login_required
def edit_profile (request):
	# register the new user
	if request.method == 'POST':
		form = ProfileForm (request.POST, request.FILES)
		
		if form.is_valid():
			user = request.user
			user.full_name = form.cleaned_data['full_name']

			profile = user.profile
			profile.business	= form.cleaned_data['business']
			profile.about_me	= form.cleaned_data['about_me']
			profile.states		= form.cleaned_data['states']
			profile.skills		= form.cleaned_data['skills']
			profile.industries	= form.cleaned_data['industries']

			if form.cleaned_data["logo"] or request.POST.has_key("logo-clear"):
				profile.logo = form.cleaned_data['logo']

			if form.cleaned_data["photo"] or request.POST.has_key("photo-clear"):
				profile.photo = form.cleaned_data['photo']

			user.save()
			profile.save()

			return redirect("coaches.views.profile", profile.id)

	else:
		user = request.user
		form = ProfileForm(initial={
			"full_name": user.full_name,
			"business": user.profile.business,
			"about_me": user.profile.about_me,
			"states": user.profile.states.all,
			"skills": user.profile.skills.all,
			"industries": user.profile.industries.all,
			"logo": user.profile.logo,
			"photo": user.profile.photo,
		})

	return TemplateResponse (request, "coaches/edit_profile.html", {"form": form})