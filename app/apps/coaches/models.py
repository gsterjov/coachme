from django.conf import settings
from django.db import models
from django.contrib.contenttypes import generic
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from django.dispatch import receiver, Signal
from django.db.models.signals import post_save
from guardian.shortcuts import assign_perm

from sorl import thumbnail

from core.signals import profile_associated
from core.utils import generate_media_path


def get_upload_path (instance, filename):
	return generate_media_path (settings.COACHES_MEDIA_IMAGE_DIR, filename)


class State (models.Model):
	name = models.CharField (max_length=30)
	def __unicode__ (self):
		return self.name

class Skill (models.Model):
	name = models.CharField (max_length=30)
	def __unicode__ (self):
		return self.name

class Qualification (models.Model):
	name = models.CharField (max_length=30)
	def __unicode__ (self):
		return self.name

class Industry (models.Model):
	name = models.CharField (max_length=30)
	def __unicode__ (self):
		return self.name

	class Meta:
		verbose_name_plural = "industries"


class Coach (models.Model):
	auth_user = generic.GenericRelation (settings.AUTH_USER_MODEL)

	business = models.CharField (null=True, max_length=50)
	about_me = models.TextField (null=True, max_length=500)

	logo	= thumbnail.ImageField (null=True, upload_to=get_upload_path)
	photo	= thumbnail.ImageField (null=True, upload_to=get_upload_path)

	logo_width		= models.IntegerField (null=True)
	logo_height		= models.IntegerField (null=True)

	photo_width		= models.IntegerField (null=True)
	photo_height	= models.IntegerField (null=True)

	states			= models.ManyToManyField (State)
	skills			= models.ManyToManyField (Skill)
	qualifications	= models.ManyToManyField (Qualification)
	industries		= models.ManyToManyField (Industry)

	updated = models.DateTimeField (auto_now=True)
	created = models.DateTimeField (auto_now_add=True)
	
	@property
	def rating (self):
		score = 0.0

		for rating in self.rating_set.all():
			score += rating.score

		if self.rating_set.count() < 5:
			return -1
		
		return score / self.rating_set.count()
	
	@property
	def user (self):
		return self.auth_user.get()

	class Meta:
		verbose_name_plural = "coaches"


class Rating (models.Model):
	user = models.ForeignKey (settings.AUTH_USER_MODEL)
	coach = models.ForeignKey (Coach)
	score = models.IntegerField (default=0)

	updated = models.DateTimeField (auto_now=True)
	created = models.DateTimeField (auto_now_add=True)

	def __unicode__ (self):
		return self.name


class Testimonial (models.Model):
	coach = models.ForeignKey (Coach)
	name = models.CharField (max_length=50)
	business = models.CharField (max_length=100)
	content = models.TextField (max_length=500)

	updated = models.DateTimeField (auto_now=True)
	created = models.DateTimeField (auto_now_add=True)




@receiver (profile_associated, sender=get_user_model())
def on_profile_associated (sender, user=None, profile=False, **kwargs):
	if user.is_coach:
		group = Group.objects.get (name="Coaches")
		user.groups.add (group)

		assign_perm ("add_coach", user, profile)
		assign_perm ("change_coach", user, profile)
		assign_perm ("delete_coach", user, profile)