from django.db.models import Count
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import UploadedFile

from tastypie import fields
from tastypie.bundle import Bundle
from tastypie.resources import Resource, ModelResource
from tastypie.authentication import MultiAuthentication, SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.validation import CleanedDataFormValidation
from tastypie.exceptions import ImmediateHttpResponse

from sorl.thumbnail import get_thumbnail

from core.api.authentication import BypassAuthentication
from core.api.authorization import GuardianAuthorization
from core.api.resources import MultipartResource, SearchResource

from coaches.models import State, Skill, Industry, Testimonial, Coach
from coaches.forms import CoachForm, TestimonialForm
from coaches.api.validation import CoachValidation


from django.conf.urls import url
from tastypie.utils import trailing_slash


class StateResource (ModelResource):
	'''
	State tag
	'''
	id = fields.CharField (attribute="id")
	name = fields.CharField (attribute="name")

	class Meta:
		queryset = State.objects.all()
		allowed_methods = ["get"]


class SkillResource (ModelResource):
	'''
	Skill tag
	'''
	id = fields.CharField (attribute="id")
	name = fields.CharField (attribute="name")

	class Meta:
		queryset = Skill.objects.all()
		allowed_methods = ["get"]


class IndustryResource (ModelResource):
	'''
	Industry tag
	'''
	id = fields.CharField (attribute="id")
	name = fields.CharField (attribute="name")

	class Meta:
		queryset = Industry.objects.all()
		allowed_methods = ["get"]



class TestimonialResource (ModelResource):
	'''
	The testimonial resource
	'''
	id = fields.CharField (attribute="id")
	name = fields.CharField (attribute="name")
	business = fields.CharField (attribute="business")
	content = fields.CharField (attribute="content")
	coach = fields.ToOneField ("coaches.api.resources.CoachResource", "coach")

	class Meta:
		queryset = Testimonial.objects.all()

		list_allowed_methods = ["get", "post", "patch"]
		detail_allowed_methods = ["get", "put"]

		validation = CleanedDataFormValidation (form_class=TestimonialForm)
		authentication = SessionAuthentication()
		authorization = GuardianAuthorization (model_class=Testimonial, view=False)



class CoachResource (MultipartResource, SearchResource, ModelResource):
	'''
	The coach resource
	'''
	id = fields.CharField (attribute="id")
	full_name = fields.CharField (null=True, readonly=True)
	business = fields.CharField (attribute="business", null=True)
	about_me = fields.CharField (attribute="about_me", null=True)
	rating = fields.CharField (attribute="rating", null=True, readonly=True)
	
	photo = fields.FileField (attribute="photo", null=True)
	photo_small = fields.FileField()
	photo_medium = fields.FileField()
	photo_large = fields.FileField()


	# Relations
	testimonials = fields.ToManyField (TestimonialResource, "testimonial_set", related_name="coach", null=True, full=True)

	states = fields.ToManyField (StateResource, "states", full=True)
	skills = fields.ToManyField (SkillResource, "skills", full=True)
	industries = fields.ToManyField (IndustryResource, "industries", full=True)

	class Meta:
		fields = ["id", "full_name" "business", "about_me", "states", "skills", "industries", "testimonials", "photo", "photo_small", "photo_medium", "photo_large"]
		queryset = Coach.objects.all()

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get", "put", "patch"]

		validation = CoachValidation()
		bypass_auth = BypassAuthentication (methods=["get", "post"])
		authentication = MultiAuthentication (bypass_auth, SessionAuthentication())
		authorization = GuardianAuthorization (model_class=Coach, view=False, add=False)


	def prepend_urls (self):
		urls = super(CoachResource, self).prepend_urls()
		return [
			url(r"^(?P<resource_name>%s)/latest%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view("get_latest")),
			url(r"^(?P<resource_name>%s)/statistics%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view("get_statistics")),
		] + urls


	def dispatch (self, request_type, request, **kwargs):
		if kwargs.has_key("pk") and kwargs["pk"] == "me":
			if request.user.is_coach:
				kwargs["pk"] = request.user.profile.id

		return super(CoachResource, self).dispatch (request_type, request, **kwargs)


	def obj_create (self, bundle):
		bundle = super(CoachResource, self).obj_create (bundle)

		user = get_user_model().objects.create_user (bundle.data["email"], bundle.data["password"])

		user.first_name = bundle.data["first_name"]
		user.last_name = bundle.data["last_name"]
		user.profile = bundle.obj
		user.save()

		return bundle


	def dehydrate_full_name (self, bundle):
		return bundle.obj.user.full_name

	def dehydrate_photo_small (self, bundle):
		return self.generate_thumbnail (bundle, "35x35")

	def dehydrate_photo_medium (self, bundle):
		return self.generate_thumbnail (bundle, "100x100")

	def dehydrate_photo_large (self, bundle):
		return self.generate_thumbnail (bundle, "200x150")


	def hydrate_photo (self, bundle):
		# only file uploads should be processed
		if bundle.data.has_key("photo") and not isinstance(bundle.data["photo"], UploadedFile):
			del bundle.data["photo"]

		return bundle


	def hydrate_skills (self, bundle):
		# hydrate happens during a PATCH so we have to assume that skills can either be
		# a name or an already loaded bundle resource
		skills = [skill for skill in bundle.data["skills"] if isinstance(skill, Bundle)]
		names = [skill["name"] for skill in bundle.data["skills"] if not isinstance(skill, Bundle)]

		# add new skills
		if len(names) > 0:
			skills += Skill.objects.filter(name__in=names)

		bundle.data["skills"] = skills
		return bundle


	def hydrate_industries (self, bundle):
		# hydrate happens during a PATCH so we have to assume that industries can either be
		# a name or an already loaded bundle resource
		industries = [industry for industry in bundle.data["industries"] if isinstance(industry, Bundle)]
		names = [industry["name"] for industry in bundle.data["industries"] if not isinstance(industry, Bundle)]

		# add new industries
		if len(names) > 0:
			industries += Industry.objects.filter(name__in=names)

		bundle.data["industries"] = industries
		return bundle


	def generate_thumbnail (self, bundle, size):
		photo = None

		if bundle.obj.photo:
			photo = get_thumbnail (bundle.obj.photo, size, crop="center", quality=99).url

		return photo


	def get_latest (self, request, **kwargs):
		self.method_check (request, allowed=["get"])
		self.is_authenticated (request)
		self.throttle_check (request)

		coaches = Coach.objects.order_by("-created").exclude (photo="")

		objects = []

		for coach in coaches:
			bundle = self.build_bundle (obj=coach, request=request)
			bundle = self.full_dehydrate (bundle)
			objects.append (bundle)

		object_list = {
			"objects": objects,
		}

		self.log_throttled_access (request)
		return self.create_response (request, object_list)


	def get_statistics (self, request, **kwargs):
		self.method_check (request, allowed=["get"])
		self.is_authenticated (request)
		self.throttle_check (request)

		industries = Industry.objects.aggregate (Count("coach"))
		skills = Skill.objects.aggregate (Count("coach"))
		states = State.objects.aggregate (Count("coach"))

		object_list = {
			"industries": industries["coach__count"],
			"skills":  skills["coach__count"],
			"states":  states["coach__count"],
			"jobs": 0,
		}

		self.log_throttled_access (request)
		return self.create_response (request, object_list)