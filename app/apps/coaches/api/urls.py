from django.conf.urls import patterns, include, url
from tastypie.api import Api

from resources import StateResource, SkillResource, IndustryResource, TestimonialResource, CoachResource


coaches_api = Api (api_name='coaches')

coaches_api.register (StateResource())
coaches_api.register (SkillResource())
coaches_api.register (IndustryResource())
coaches_api.register (TestimonialResource())
coaches_api.register (CoachResource())