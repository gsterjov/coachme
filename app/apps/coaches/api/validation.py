from django.contrib.auth import get_user_model

from tastypie.validation import Validation

from coaches.forms import CoachForm, ProfileForm
from coaches.models import Coach


class CoachValidation (Validation):
	'''
	Custom validation to make sure relationships are unique
	'''
	def is_valid (self, bundle, request=None):
		errors = {}

		form = CoachForm() if bundle.obj.pk is None else ProfileForm()

		if form.is_valid():
			# update with cleaned values
			for key, value in form.cleaned_data.iteritems():
				bundle.data[key] = value

			# only check email for new coaches, not ones being updated
			if bundle.obj.pk is None:
				if get_user_model().objects.filter(email=bundle.data["email"]).exists():
					errors["email"] = ["This email already exists"]

		else:
			errors = form.errors

		return errors