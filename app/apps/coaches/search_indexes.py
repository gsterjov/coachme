from haystack.indexes import SearchIndex, Indexable, CharField, MultiValueField
from coaches.models import Coach

class CoachIndex (SearchIndex, Indexable):
	text = CharField (document=True, use_template=True)

	states			= MultiValueField()
	skills			= MultiValueField()
	qualifications	= MultiValueField()
	industries		= MultiValueField()

	def get_model (self):
		return Coach

	def prepare_states (self, obj):
		return [state.id for state in obj.states.all()]

	def prepare_skills (self, obj):
		return [skill.id for skill in obj.skills.all()]

	def prepare_qualifications (self, obj):
		return [qualification.id for qualification in obj.qualifications.all()]

	def prepare_industries (self, obj):
		return [industry.id for industry in obj.industries.all()]