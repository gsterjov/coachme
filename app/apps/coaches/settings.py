# The uploaded images directory relative to MEDIA_ROOT.
# This can be formatted with python's date-time module.
COACHES_MEDIA_IMAGE_DIR = 'coaches/images/%Y/%m/%d'