from django.conf.urls import patterns, include, url

urlpatterns = patterns('coaches.views',
	url(r'create$', 'create'),
	url(r'list$', 'list'),

	url(r'profile/edit$', 'edit_profile'),
	url(r'profile/(\d)$', 'profile'),

	url(r'search$', 'search'),
	# (r'^search/', include('haystack.urls')),
)