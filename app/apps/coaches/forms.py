from django import forms
from coaches.models import State, Skill, Industry


class CoachForm (forms.Form):
	first_name	= forms.CharField (max_length=100, error_messages={'required': 'Please enter your first name'})
	last_name	= forms.CharField (max_length=100, error_messages={'required': 'Please enter your last name'})
	password	= forms.CharField (max_length=20, error_messages={'required': 'Please enter a password'}, widget=forms.PasswordInput())
	
	email = forms.EmailField (error_messages={
		'required': 'Please enter your email address',
		'invalid': 'An email address must be of the form "example@email.com"'
	})


class ProfileForm (forms.Form):
	business	= forms.CharField (max_length=50)
	about_me	= forms.CharField (max_length=500, required=False, widget=forms.Textarea)
	photo		= forms.ImageField (required=False)

	states		= forms.ModelMultipleChoiceField (required=False, queryset=State.objects.all())
	skills		= forms.ModelMultipleChoiceField (required=False, queryset=Skill.objects.all())
	industries	= forms.ModelMultipleChoiceField (required=False, queryset=Industry.objects.all())


class TestimonialForm (forms.Form):
	name = forms.CharField (max_length=50)
	business = forms.CharField (max_length=100)
	content = forms.CharField (max_length=500, widget=forms.Textarea)


class SearchForm (forms.Form):
	q = forms.CharField (max_length=100, required=False)

	states		= forms.ModelMultipleChoiceField (required=False, queryset=State.objects.all())
	skills		= forms.ModelMultipleChoiceField (required=False, queryset=Skill.objects.all())
	industries	= forms.ModelMultipleChoiceField (required=False, queryset=Industry.objects.all())