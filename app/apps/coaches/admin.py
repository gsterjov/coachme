from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from coaches.models import Coach, State, Skill, Qualification, Industry


class CoachAdmin (AdminImageMixin, admin.ModelAdmin):
	list_display = ("full_name", "email", "business")
	fields = ("business", "about_me", "logo", "photo")

	def full_name (self, obj):
		return obj.user.full_name

	def email (self, obj):
		return obj.user.email


class StateAdmin (admin.ModelAdmin):
	list_display = ("name",)

class SkillAdmin (admin.ModelAdmin):
	list_display = ("name",)

class QualificationAdmin (admin.ModelAdmin):
	list_display = ("name",)

class IndustryAdmin (admin.ModelAdmin):
	list_display = ("name",)


admin.site.register (Coach, CoachAdmin)
admin.site.register (State, StateAdmin)
admin.site.register (Skill, SkillAdmin)
admin.site.register (Qualification, QualificationAdmin)
admin.site.register (Industry, IndustryAdmin)