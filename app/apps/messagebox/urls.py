from django.conf.urls import patterns, include, url

urlpatterns = patterns('messagebox.views',
	url(r'inbox$', 'inbox'),
	url(r'create/(\d)$', 'message'),
	url(r'read/(\d)$', 'read'),
)