from django.conf import settings
from django.http import HttpResponseForbidden
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from core.utils import TemplateResponse

from models import Message
from forms import CreateMessage


@login_required
def inbox (request):
	messages = request.user.received_messages.all()
	return TemplateResponse (request, "messagebox/inbox.html", {"messages": messages})


@login_required
def message (request, recipient_id):
	recipient = get_object_or_404 (settings.AUTH_USER_MODEL, pk=recipient_id)

	# create the message
	if request.method == 'POST':
		form = CreateMessage (request.POST)
		
		if form.is_valid():
			message = Message()

			message.sender = request.user
			message.recipient = recipient
			message.subject = form.cleaned_data["subject"]
			message.body = form.cleaned_data["body"]

			message.save()
			return redirect("coaches.views.profile", recipient.profile.id)

	else:
		form = CreateMessage()

	return TemplateResponse (request, "messagebox/create.html", {"recipient": recipient, "form": form})


@login_required
def read (request, message_id):
	message = get_object_or_404 (Message, pk=message_id)
	
	# basic permission check
	if message.recipient == request.user:
		return TemplateResponse (request, "messagebox/read.html", {"message": message})

	return HttpResponseForbidden()