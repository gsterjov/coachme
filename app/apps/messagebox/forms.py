from django import forms

class CreateMessage (forms.Form):
	subject	= forms.CharField (max_length=50, required=False)
	body	= forms.CharField (required=False, widget=forms.Textarea())