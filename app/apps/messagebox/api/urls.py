from django.conf.urls import patterns, include, url
from tastypie.api import Api

from resources import MessageResource


messagebox_api = Api (api_name='messages')

messagebox_api.register (MessageResource())