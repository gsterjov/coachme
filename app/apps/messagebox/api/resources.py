from django.contrib.auth import get_user_model

from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.validation import CleanedDataFormValidation

from tastypie.http import HttpUnauthorized
from tastypie.exceptions import ImmediateHttpResponse

from core.api.authorization import GuardianAuthorization
from core.api.resources import UserResource

from core.models import User, Relationship
from messagebox.models import Message
from messagebox.forms import CreateMessage



class MessageResource (ModelResource):
	sender = fields.ToOneField (UserResource, "sender")
	recipient = fields.ToOneField (UserResource, "recipient")

	subject = fields.CharField (attribute="subject", null=True)
	body = fields.CharField (attribute="body", null=True)

	sent_at = fields.DateField (attribute="sent_at", null=True)
	read_at = fields.DateField (attribute="read_at", null=True)

	class Meta:
		queryset = Message.objects.all()
		fields = ["sender", "recipient", "subject", "body", "sent_at", "read_at"]

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get"]

		validation = CleanedDataFormValidation (form_class=CreateMessage)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()


	def authorized_read_list (self, objects, bundle):
		return objects.filter(recipient=bundle.request.user)
	

	def obj_create (self, bundle):
		relationship = bundle.request.user.get_relationship (bundle.data["recipient"])

		if not relationship or relationship.status != Relationship.ACCEPTED:
			raise ImmediateHttpResponse (HttpUnauthorized())

		return super(MessageResource, self).obj_create (bundle)


	def hydrate (self, bundle):
		bundle.obj.sender = bundle.request.user
		return bundle

	def hydrate_recipient (self, bundle):
		bundle.data["recipient"] = get_user_model().objects.get (pk=bundle.data["recipient"])
		return bundle