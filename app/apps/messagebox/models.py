from django.db import models
from django.conf import settings
from django.utils.timezone import now


class Message (models.Model):
	sender		= models.ForeignKey (settings.AUTH_USER_MODEL, related_name='sent_messages')
	recipient	= models.ForeignKey (settings.AUTH_USER_MODEL, related_name='received_messages')

	subject		= models.CharField (blank=True, max_length=30)
	body		= models.TextField (blank=True)

	sent_at = models.DateTimeField (default=now)
	read_at = models.DateTimeField (null=True)