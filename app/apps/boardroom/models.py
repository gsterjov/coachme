import datetime

from django.db import models
from django.utils.timezone import now
from django.conf import settings

from django.dispatch import receiver
from django.db.models.signals import post_save
from events.signals import create_event, delete_event


class Project (models.Model):
	'''
	A project is essentially a group of tasks, requirements,
	and other management tools.
	'''
	owner		= models.ForeignKey (settings.AUTH_USER_MODEL)
	
	title		= models.CharField (max_length=100)
	description	= models.TextField (null=True, max_length=500)

	added_at	= models.DateTimeField (default=now)
	updated_at	= models.DateTimeField (null=True)

	def __unicode__ (self):
		return self.title

	class Meta:
		permissions = (
			("view_project", "Can view projects"),
		)



class Brief (models.Model):
	'''
	A brief which encapsulates the project requirements
	'''
	project		= models.OneToOneField (Project)
	description	= models.TextField (null=True, max_length=500)

	created_at	= models.DateTimeField (default=now)
	updated_at	= models.DateTimeField (null=True)



class Task (models.Model):
	'''
	A project task assigned to a user
	'''
	project		= models.ForeignKey (Project)
	owner		= models.ForeignKey (settings.AUTH_USER_MODEL)
	
	title		= models.CharField (max_length=100)
	description	= models.TextField (null=True, max_length=500)

	due_date	= models.DateField (null=True)

	added_at	= models.DateTimeField (default=now)
	updated_at	= models.DateTimeField (null=True)



class Milestone (models.Model):
	'''
	A project milestone
	'''
	project		= models.ForeignKey (Project)
	owner		= models.ForeignKey (settings.AUTH_USER_MODEL)
	
	title		= models.CharField (max_length=100)
	description	= models.TextField (null=True, max_length=500)

	due_date	= models.DateField()

	added_at	= models.DateTimeField (default=now)
	updated_at	= models.DateTimeField (null=True)



@receiver (post_save, sender=Project)
def on_project_saved (sender, instance=None, created=False, **kwargs):
	if created:
		brief = Brief()
		brief.project = instance
		brief.save()


@receiver (post_save, sender=Task)
def on_task_saved (sender, instance=None, **kwargs):
	if instance.due_date:
		start = datetime.datetime.combine (instance.due_date, datetime.time.min)
		end = datetime.datetime.combine (instance.due_date, datetime.time.max)

		create_event.send (Task, user=instance.owner, regarding=instance, start=start, end=end)
	
	else:
		delete_event.send (Task, regarding=instance)


@receiver (post_save, sender=Milestone)
def on_milestone_saved (sender, instance=None, **kwargs):
	start = datetime.datetime.combine (instance.due_date, datetime.time.min)
	end = datetime.datetime.combine (instance.due_date, datetime.time.max)

	create_event.send (Milestone, user=instance.owner, regarding=instance, start=start, end=end)