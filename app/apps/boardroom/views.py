from django.conf import settings
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from guardian.shortcuts import assign_perm

from core.utils import TemplateResponse, JsonResponse

import forms
from models import Project, Brief, Task, Milestone


@login_required
def index (request):
	return TemplateResponse (request, "boardroom/index.html")


@login_required
def list_boardrooms (request):
	return TemplateResponse (request, "boardroom/core/list.html")


@login_required
def create_boardroom (request):
	# create the new boardroom
	if request.method == 'POST':
		form = forms.CreateBoardroom (request.POST)
		
		if form.is_valid():
			# create boardroom
			boardroom = Boardroom()
			boardroom.owner = request.user
			boardroom.title = form.cleaned_data['title']
			boardroom.description = form.cleaned_data['description']
			boardroom.save()
			return redirect ("boardroom.views.boardroom_projects", boardroom.id)

	else:
		form = forms.CreateBoardroom()

	return TemplateResponse (request, "boardroom/core/create.html", {"form": form})


@login_required
def boardroom_projects (request, boardroom_id):
	boardroom = get_object_or_404 (Boardroom, pk=boardroom_id)
	return TemplateResponse (request, "boardroom/core/projects.html", {"boardroom": boardroom})


@login_required
def boardroom_invite (request, boardroom_id):
	boardroom = get_object_or_404 (Boardroom, pk=boardroom_id)

	if request.method == "POST":
		form = forms.Invite (boardroom, request.POST)

		if form.is_valid():
			invitee = get_object_or_404 (get_user_model(), email=form.cleaned_data['email'])
			boardroom.users.add (invitee)

	else:
		form = forms.Invite (boardroom)

	return TemplateResponse (request, "boardroom/core/invite.html", {"boardroom": boardroom, "form": form})



@login_required
def create_project (request, boardroom_id):
	boardroom = get_object_or_404 (Boardroom, pk=boardroom_id)

	# create the new project
	if request.method == 'POST':
		form = forms.CreateProject (request.POST)
		
		if form.is_valid():
			# create project
			project = Project()
			project.boardroom	= boardroom
			project.title		= form.cleaned_data['title']
			project.description	= form.cleaned_data['description']
			project.save()

			assign_perm ('view_project', request.user, project)
			return redirect ("boardroom.views.boardroom_projects", boardroom_id)

	else:
		form = forms.CreateProject()

	return TemplateResponse (request, "boardroom/projects/create.html", {"form": form})


@login_required
def project_overview (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)
	return TemplateResponse (request, "boardroom/projects/overview.html", {"project": project})


@login_required
def project_brief (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)
	return TemplateResponse (request, "boardroom/projects/brief.html", {"project": project})


@login_required
def project_tasks (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)
	return TemplateResponse (request, "boardroom/projects/tasks.html", {"project": project})


@login_required
def project_milestones (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)
	return TemplateResponse (request, "boardroom/projects/milestones.html", {"project": project})


@login_required
def project_whiteboard (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)
	return TemplateResponse (request, "boardroom/projects/whiteboard.html", {"project": project})




@login_required
def edit_brief (request, brief_id):
	brief = get_object_or_404 (Brief, pk=brief_id)

	if request.method == "POST":
		form = forms.EditBrief (request.POST)
		
		if form.is_valid():
			brief.description = form.cleaned_data["description"]
			brief.save()

			return redirect ("boardroom.views.project_brief", brief.project.id)

	else:
		form = forms.EditBrief(initial={
			"description": brief.description,
		})

	return TemplateResponse (request, "boardroom/briefs/edit.html", {"form": form, "project": brief.project})



@login_required
def create_task (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)
	
	# create the new task
	if request.method == "POST":
		form = forms.CreateTask (request.POST)
		
		if form.is_valid():

			# create task
			task = Task()
			task.user			= request.user
			task.project		= project
			task.title			= form.cleaned_data["title"]
			task.description	= form.cleaned_data["description"]
			task.due_date		= form.cleaned_data["due_date"]
			task.save()

			return redirect ("boardroom.views.view_project", project_id)

	else:
		form = forms.CreateTask()

	return TemplateResponse (request, "boardroom/tasks/create.html", {"form": form, "project": project})


@login_required
def edit_task (request, task_id):
	task = get_object_or_404 (Task, pk=task_id)

	# create the new task
	if request.method == "POST":
		form = forms.CreateTask (request.POST)
		
		if form.is_valid():
			task.title			= form.cleaned_data["title"]
			task.description	= form.cleaned_data["description"]
			task.due_date		= form.cleaned_data["due_date"]
			task.save()

			return redirect ("boardroom.views.view_project", task.project.id)

	else:
		form = forms.CreateTask(initial={
			"title": task.title,
			"description": task.description,
			"due_date": task.due_date,
		})

	return TemplateResponse (request, "boardroom/tasks/edit.html", {"form": form, "project": task.project})



@login_required
def create_milestone (request, project_id):
	project = get_object_or_404 (Project, pk=project_id)

	# create the new milestone
	if request.method == "POST":
		form = forms.CreateMilestone (request.POST)
		
		if form.is_valid():
			# create milestone
			milestone = Milestone()
			milestone.project		= project
			milestone.title			= form.cleaned_data["title"]
			milestone.description	= form.cleaned_data["description"]
			milestone.due_date		= form.cleaned_data["due_date"]
			milestone.save()

			return redirect ("boardroom.views.view_project", project_id)

	else:
		form = forms.CreateMilestone()

	return TemplateResponse (request, "boardroom/milestones/create.html", {"form": form, "project": project})


@login_required
def edit_milestone (request, milestone_id):
	milestone = get_object_or_404 (Milestone, pk=milestone_id)

	# create the new task
	if request.method == "POST":
		form = forms.CreateMilestone (request.POST)
		
		if form.is_valid():
			milestone.title			= form.cleaned_data["title"]
			milestone.description	= form.cleaned_data["description"]
			milestone.due_date		= form.cleaned_data["due_date"]
			milestone.save()

			return redirect ("boardroom.views.view_project", milestone.project.id)

	else:
		form = forms.CreateMilestone(initial={
			"title": milestone.title,
			"description": milestone.description,
			"due_date": milestone.due_date,
		})

	return TemplateResponse (request, "boardroom/milestones/edit.html", {"form": form, "project": milestone.project})


@login_required
def conference (request, boardroom_id):
	boardroom = get_object_or_404 (Boardroom, pk=boardroom_id)
	return TemplateResponse (request, "boardroom/conference/index.html", {"boardroom": boardroom})
