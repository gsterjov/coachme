from django import forms
from core.widgets import DatePicker, ChosenSelectMultiple
from models import Project


class CreateProject (forms.Form):
	title		= forms.CharField (max_length=100)
	description	= forms.CharField (max_length=500, required=False, widget=forms.Textarea)


class CreateTask (forms.Form):
	title		= forms.CharField (max_length=100)
	description	= forms.CharField (max_length=500, required=False, widget=forms.Textarea)
	due_date	= forms.DateField (required=False, widget=DatePicker)


class CreateMilestone (forms.Form):
	title		= forms.CharField (max_length=100)
	description	= forms.CharField (max_length=500, required=False, widget=forms.Textarea)
	due_date	= forms.DateField (widget=DatePicker)


class EditBrief (forms.Form):
	description	= forms.CharField (max_length=500, required=False, widget=forms.Textarea)


class Invite (forms.Form):

	def __init__ (self, boardroom, *args, **kwargs):
		super(Invite, self).__init__ (*args, **kwargs)
		self.fields["permitted_projects"].queryset = boardroom.project_set.all()

	email = forms.EmailField (required=True)
	permit_all_projects = forms.BooleanField (required=False)
	permitted_projects = forms.ModelMultipleChoiceField (required=False, queryset=Project.objects.none(), widget=ChosenSelectMultiple)