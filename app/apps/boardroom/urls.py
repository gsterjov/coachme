from django.conf.urls import patterns, include, url

urlpatterns = patterns('boardroom.views',

	# projects
	url(r'project/create/(\d)$', 'create_project'),

	url(r'project/(\d)/overview/$', 'project_overview'),
	url(r'project/(\d)/brief/$', 'project_brief'),
	url(r'project/(\d)/tasks/$', 'project_tasks'),
	url(r'project/(\d)/milestones/$', 'project_milestones'),
	url(r'project/(\d)/whiteboard/$', 'project_whiteboard'),

	# briefs
	url(r'brief/(\d)/edit/$', 'edit_brief'),

	# tasks
	url(r'task/create/(\d)$', 'create_task'),
	url(r'task/(\d)/edit/$', 'edit_task'),

	# milestones
	url(r'milestone/create/(\d)$', 'create_milestone'),
	url(r'milestone/(\d)/edit/$', 'edit_milestone'),

	# conference
	url(r'(\d)/conference$', 'conference'),

	# core
	url(r'list/$', 'list_boardrooms'),
	url(r'create/$', 'create_boardroom'),
	url(r'(\d)/projects$', 'boardroom_projects'),
	url(r'(\d)/invite$', 'boardroom_invite'),

	url(r'', 'index'),
)