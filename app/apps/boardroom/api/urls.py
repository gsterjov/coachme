from django.conf.urls import patterns, include, url
from tastypie.api import Api

from resources import ProjectResource, BriefResource, MilestoneResource, TaskResource


boardroom_api = Api (api_name='boardroom')

boardroom_api.register (ProjectResource())
boardroom_api.register (BriefResource())
boardroom_api.register (MilestoneResource())
boardroom_api.register (TaskResource())