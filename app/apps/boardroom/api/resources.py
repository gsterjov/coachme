from django.conf import settings
from django.contrib.auth import get_user_model

from tastypie import fields
from tastypie.resources import Resource, ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.validation import CleanedDataFormValidation

from boardroom.models import Project, Brief, Milestone, Task
from boardroom.forms import CreateProject, EditBrief, CreateMilestone, CreateTask

from core.api.resources import UserResource


class ProjectResource (ModelResource):
	id = fields.CharField (attribute='id')
	title = fields.CharField (attribute="title")
	description = fields.CharField (attribute="description")
	created_at = fields.DateTimeField (attribute="added_at")

	# Relations
	owner = fields.ToOneField (UserResource, "owner")
	brief = fields.ToOneField ("boardroom.api.resources.BriefResource", "brief", readonly=True, related_name="project")

	class Meta:
		queryset = Project.objects.all()
		fields = ["id", "title", "description", "created_at"]

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get"]

		validation = CleanedDataFormValidation (form_class=CreateProject)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()

	def hydrate (self, bundle):
		bundle.obj.owner = bundle.request.user
		return bundle

	def authorized_read_list (self, objects, bundle):
		return objects.filter(owner=bundle.request.user)


class BriefResource (ModelResource):
	project = fields.ToOneField (ProjectResource, "project")
	description = fields.CharField (attribute="description", null=True)

	class Meta:
		queryset = Brief.objects.all()
		fields = ["description"]

		list_allowed_methods = []
		detail_allowed_methods = ["get"]

		validation = CleanedDataFormValidation (form_class=EditBrief)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()

	def authorized_read_list (self, objects, bundle):
		return objects.filter(project__owner=bundle.request.user)


class MilestoneResource (ModelResource):
	project = fields.ToOneField (ProjectResource, "project")
	owner = fields.ToOneField (UserResource, "owner")

	class Meta:
		queryset = Milestone.objects.all()
		fields = ["title", "description", "due_date"]

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get"]

		filtering = {
			"project": ('exact',)
		}

		validation = CleanedDataFormValidation (form_class=CreateMilestone)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()

	def hydrate (self, bundle):
		bundle.obj.owner = bundle.request.user
		return bundle

	def authorized_read_list (self, objects, bundle):
		return objects.filter(owner=bundle.request.user)


class TaskResource (ModelResource):
	project = fields.ToOneField (ProjectResource, "project")
	owner = fields.ToOneField (UserResource, "owner")

	class Meta:
		queryset = Task.objects.all()
		fields = ["title", "description", "due_date"]

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get"]

		filtering = {
			"project": ('exact',)
		}

		validation = CleanedDataFormValidation (form_class=CreateTask)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()

	def hydrate (self, bundle):
		bundle.obj.owner = bundle.request.user
		return bundle

	def authorized_read_list (self, objects, bundle):
		return objects.filter(owner=bundle.request.user)