from oauthlib.oauth2.rfc6749.errors import MismatchingStateError
from requests_oauthlib import OAuth2Session

import time
from datetime import timedelta
from django.utils.timezone import now


class OAuth2Provider (object):
	"""
	A generic OAuth2 provider that forces strict state checking
	to prevent CSRF attacks.

	To properly inherit this class all subclasses must define `auth_url` and `token_url`
	as they will be used as endpoints in their respective requests.
	"""

	def __init__ (self, client, redirect_uri=None, state=None):
		"""
		Create the provider.

		:param client: The client details to use when making requests to the provider.
		:param redirect_uri: The URI to redirect the user back to once authorised. The URI
		                     will be passed a state and code to retrieve a token with.
		:param state: The original request state to use when retrieving tokens.
		"""
		self.client = client
		self.redirect_uri = redirect_uri
		self.state = state


	def authorisation_url (self, scope, **kwargs):
		"""
		Get the URL to authorise the client.

		:param scope: The resource scope to request access for.
		:param kwargs: Extra parameters to pass on to the authorisation request.

		:return: A tuple containing the authorisation URL and the state used in the request.
		"""
		oauth = OAuth2Session (self.client.client_id, redirect_uri=self.redirect_uri, scope=scope)
		url, self.state = oauth.authorization_url (self.auth_url, **kwargs)
		return url, self.state


	def token (self, state, code, **kwargs):
		"""
		Retrieve the token to access resources. This should be called
		once the authorisation code has been returned through the redirect URI.

		:param state: The state returned by the redirect URI. If it doesn't match the state
		              provided when requesting authorisation then a MismatchingStateError will be raised.
		:param code: The authorisation code returned by the redirect URI.
		:param kwargs: Extra parameters to pass on to the token request.

		:return: A dictionary containing the access token and expiry amongst other things.
		"""
		if state != self.state:
			raise MismatchingStateError()

		oauth = OAuth2Session (self.client.client_id, redirect_uri=self.redirect_uri, state=self.state)
		token = oauth.fetch_token (self.token_url, code=code, **kwargs)

		# create a timezone aware expiry
		if token.has_key ("expires_in"):
			token["expires_at"] = now() + timedelta (seconds=token["expires_in"])

		return token


	def post (self, token, url, data, **kwargs):
		token = {
			"access_token": token.access,
			# "expires_in": token.expires_in,
			# "expires_at": time.mktime (token.expires_at.timetuple()),
		}

		oauth = OAuth2Session (self.client.client_id, token=token)
		return oauth.post (url, data, **kwargs)



class GoogleProvider (OAuth2Provider):
	"""
	An OAuth2 provider for Google services.
	"""
	auth_url = "https://accounts.google.com/o/oauth2/auth"
	token_url = "https://accounts.google.com/o/oauth2/token"

	def token (self, state, code):
		return super(GoogleProvider, self).token (state, code, client_secret=self.client.secret)