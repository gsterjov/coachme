from django.contrib import admin

from models import Client, Account, Token


class ClientAdmin (admin.ModelAdmin):
    list_display = ("name", "provider")


class AccountAdmin (admin.ModelAdmin):
    list_display = ("user", "client")
    list_filter = ("client",)


class TokenAdmin (admin.ModelAdmin):
    list_display = ("account", "name", "expires_at")
    list_filter = ("name", "expires_at")



admin.site.register (Client, ClientAdmin)
admin.site.register (Account, AccountAdmin)
admin.site.register (Token, TokenAdmin)