from django.conf import settings
from django.db import models

import providers


class Client (models.Model):
	'''
	Client details for a 3rd party authorisation provider.
	Contains app specific configuration like client IDs and oauth secrets.
	'''
	PROVIDERS = (
		("google", "Google"),
		("facebook", "Facebook"),
		("twitter", "Twitter"),
		("linkedin", "LinkedIn"),
	)

	name = models.CharField (max_length=50)
	provider = models.CharField (max_length=20, choices=PROVIDERS)

	client_id = models.CharField (max_length=100)
	secret = models.CharField (max_length=100)


	def __str__ (self):
		return self.name


class Account (models.Model):
	'''
	An external web account.
	The association between a user account and a 3rd party auth provider.
	'''
	user = models.ForeignKey (settings.AUTH_USER_MODEL, related_name='web_accounts')
	client = models.ForeignKey (Client)

	def __str__ (self):
		return "{0} ({1})".format (self.user.full_name, self.client.name)


class Token (models.Model):
	'''
	The web account token.

	A token is the outcome of privileges granted to the webapp once it has
	been connected to an auth provider. A user can have many tokens for a
	provider to facilitate incremental authorisation.

	As such tokens must be given a name so the correct access token can be
	used for requests made to a provider resource.
	'''
	account = models.ForeignKey (Account)
	name = models.CharField (max_length=50)

	access = models.CharField (max_length=200)
	refresh = models.CharField (max_length=200, null=True)

	expires_in = models.IntegerField (null=True)
	expires_at = models.DateTimeField (null=True)


	def __str__ (self):
		return self.name