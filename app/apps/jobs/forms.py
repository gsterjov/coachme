from django import forms
from coaches.models import Industry, Skill


class JobForm (forms.Form):
	title		= forms.CharField (max_length=100, error_messages={"required": "You must enter a job name"})
	description	= forms.CharField (max_length=3000, required=False, widget=forms.Textarea)

	industry		= forms.ModelChoiceField (queryset=Industry.objects.all(), error_messages={"required": "You must select an industry"})
	desired_skills	= forms.ModelMultipleChoiceField (required=False, queryset=Skill.objects.all())

	duration = forms.IntegerField (error_messages={"required": "A job duration must be entered"})



class ProposalForm (forms.Form):
	details	= forms.CharField (max_length=4000, widget=forms.Textarea, error_messages={"required": "You must enter the details of the proposal"})
	duration = forms.IntegerField (error_messages={"required": "A job duration must be entered"})