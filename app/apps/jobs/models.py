from django.db import models
from django.utils.timezone import now
from django.conf import settings

from coaches.models import Industry, Skill


class Job (models.Model):
	'''
	A job posted by a user.
	'''
	owner = models.ForeignKey (settings.AUTH_USER_MODEL)

	title = models.CharField (max_length=100)
	description = models.TextField (null=True, max_length=3000)

	industry = models.ForeignKey (Industry)
	desired_skills = models.ManyToManyField (Skill)

	start_date = models.DateField (null=True)
	duration = models.IntegerField()

	added_at = models.DateTimeField (default=now)
	updated_at = models.DateTimeField (null=True)



class Proposal (models.Model):
	'''
	A job proposal submitted by a user.
	'''
	owner = models.ForeignKey (settings.AUTH_USER_MODEL)
	job = models.ForeignKey (Job)

	details = models.TextField (null=True, max_length=4000)

	start_date = models.DateField (null=True)
	duration = models.IntegerField()

	hourly_rate = models.FloatField (null=True)
	hours_per_week = models.FloatField (null=True)

	added_at = models.DateTimeField (default=now)
	updated_at = models.DateTimeField (null=True)