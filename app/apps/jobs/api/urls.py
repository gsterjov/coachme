from django.conf.urls import patterns, include, url
from tastypie.api import Api

from resources import JobResource, ProposalResource


jobs_api = Api (api_name='jobs')

jobs_api.register (JobResource())
jobs_api.register (ProposalResource())