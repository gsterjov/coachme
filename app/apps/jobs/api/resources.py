from django.conf.urls import url

from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.utils import trailing_slash

from jobs.models import Job, Proposal
from jobs.forms import JobForm, ProposalForm

from core.api.validation import FormValidation
from core.api.resources import UserResource
from coaches.api.resources import IndustryResource, SkillResource


class JobResource (ModelResource):
	'''
	A job resource.
	'''
	id = fields.CharField (attribute='id')
	title = fields.CharField (attribute="title")
	description = fields.CharField (attribute="description")
	duration = fields.IntegerField (attribute="duration")
	added_at = fields.DateTimeField (attribute="added_at")

	# Relations
	owner = fields.ToOneField (UserResource, "owner")

	industry = fields.ToOneField (IndustryResource, "industry", full=True)
	desired_skills = fields.ToManyField (SkillResource, "desired_skills", full=True)


	class Meta:
		queryset = Job.objects.all()
		fields = ["id", "title", "description", "duration", "added_at", "owner", "industry", "desired_skills"]

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get"]

		validation = FormValidation (form_class=JobForm)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()


	def prepend_urls (self):
		urls = super(JobResource, self).prepend_urls()
		return [
			url(r"^(?P<resource_name>%s)/recommended%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view("get_recommended")),
		] + urls


	def hydrate (self, bundle):
		bundle.obj.owner = bundle.request.user
		return bundle

	def authorized_read_list (self, objects, bundle):
		return objects.filter(owner=bundle.request.user)


	def get_recommended (self, request, **kwargs):
		self.method_check (request, allowed=["get"])
		self.is_authenticated (request)
		self.throttle_check (request)

		industries = request.user.profile.industries.all()
		jobs = Job.objects.filter(industry__in = industries)

		objects = []

		for job in jobs:
			bundle = self.build_bundle (obj=job, request=request)
			bundle = self.full_dehydrate (bundle)
			objects.append (bundle)

		object_list = {
			"objects": objects,
		}

		self.log_throttled_access (request)
		return self.create_response (request, object_list)



class ProposalResource (ModelResource):
	'''
	A job proposal resource.
	'''
	id				= fields.CharField (attribute='id')
	details			= fields.CharField (attribute="details")
	start_date		= fields.IntegerField (attribute="start_date", null=True)
	duration		= fields.IntegerField (attribute="duration")
	hourly_rate		= fields.FloatField (attribute="hourly_rate", null=True)
	hours_per_week	= fields.FloatField (attribute="hours_per_week", null=True)
	added_at		= fields.DateTimeField (attribute="added_at")

	# Relations
	owner = fields.ToOneField (UserResource, "owner")
	job = fields.ToOneField (JobResource, "job")


	class Meta:
		queryset = Proposal.objects.all()
		fields = ["id", "details", "start_date", "duration", "hourly_rate", "hours_per_week", "added_at", "owner", "job"]

		list_allowed_methods = ["get", "post"]
		detail_allowed_methods = ["get"]

		validation = FormValidation (form_class=ProposalForm)
		authentication = SessionAuthentication()
		authorization = DjangoAuthorization()


	def hydrate (self, bundle):
		bundle.obj.owner = bundle.request.user
		return bundle

	def authorized_read_list (self, objects, bundle):
		user = bundle.request.user

		if user.is_coach:
			return objects.filter (owner = user)
		else:
			return objects.filter (job__in = user.job_set.values_list("id", flat=True))