
ContactsController = ($scope, $element, Chat)->
	$scope.status = "Offline"
	$scope.contacts = Chat.contacts
	$scope.user = Chat.user


	$scope.activate = (contact)->
		$scope.active_contacts.push contact


	$scope.connect = ()->
		$scope.status = "Connecting.."
		
		Chat.connect().then (resp)->
			$scope.status = "Online" if resp.status == "online"
			$scope.status = "Offline" if resp.status == "offline"



angular.module("chat.contacts.directive", ['chat.services'])
	.directive "contacts", ()-> {
		restrict: "E",
		transclude: false,
		replace: true,
		scope: true,
		templateUrl: "boardroom/chat/templates/contacts.html",
		controller: ContactsController,
	}
