service = angular.module "chat.services", ["core.wamp.services", "resources"]


class ChatService
	constructor: (@$rootScope, @Wamp, @User, Contact)->
		@contacts = {}

		# listen to the user's channel for any new messages
		@user = User.get "me", ()=>
			@Wamp.prefix "chat", "http://coachme.com.au/chat/"
			@Wamp.prefix "chat-events", "http://coachme.com.au/chat/#{@user.id}/events#"
			@Wamp.subscribe "chat-events:messages", @_on_message

		# add accepted contacts
		contacts = Contact.query {status: "ACCEPTED"}, ()=>
			for contact in contacts
				@contacts[contact.id] = contact
				@contacts[contact.id].messages = []


	# Send a message to the specified recepient
	send: (recipient, message) ->
		@Wamp.publish "chat:#{recipient.id}/events#messages", message: message

		@contacts[recipient.id].messages.push
			message: message
			ts: moment().format("YYYY-MM-DDTHH:mm:ss")


	_on_message: (uri, ev) =>
		if ev.message?
			contact = @contacts[ev.sender]
			contact.messages.push
				message: ev.message
				ts: ev.ts

			@$rootScope.$broadcast "chat.message", contact, ev.message, ev.ts
			@$rootScope.$digest()




service.service "Chat", ["$rootScope", "Wamp", "User", "Contact", ChatService]