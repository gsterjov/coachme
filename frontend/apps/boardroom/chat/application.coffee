angular.module("chat", [
	# services
	"chat.services",

	# directives
	"chat.contacts.directive",
	"chat.chat-window.directive"
])