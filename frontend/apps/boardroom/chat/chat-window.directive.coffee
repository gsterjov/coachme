
ChatWindowController = ($scope, $element, Chat)->
	$scope.contact = Chat.contacts[$scope.contact_id]

	$scope.send = ()->
		Chat.send $scope.contact, $scope.message_input
		$scope.message_input = ""



angular.module("chat.chat-window.directive", ['chat.services'])
	.directive "chatWindow", ()-> {
		restrict: "E",
		transclude: false,
		replace: true,
		scope: {contact_id: "@contact"},
		templateUrl: "boardroom/chat/templates/chat_window.html",
		controller: ChatWindowController,
	}
