# The messages submodule
app = angular.module("boardroom.messages", [
	"ui.router",
	"ui.bootstrap",
	"resources",
])


# MessagesController
# This is the messages landing page which shows a list of messages.
app.controller "MessagesController", ($scope, Message, Contact)->
	$scope.messages = Message.query()



# MessagesNewController
# This is will create and send a message to an accepted contact.
app.controller "MessagesNewController", ($scope, $state, Message, Contact)->
	$scope.message = Message
	$scope.contacts = Contact.query status: "ACCEPTED"
	$scope.form = {}


	$scope.create = ()->
		$scope.message.post()
			.success ()->
				$state.go "^"
			.error (response)->
				$scope.errors = response


	$scope.select_contact = ()->
		$scope.message.recipient = $scope.form.message.recipient.id





# Messages Routing
# The various routes and states for the messages submodule
app.config ($stateProvider, $urlRouterProvider)->
	$stateProvider
		.state "messages",
			url: "/messages"
			templateUrl: "boardroom/partials/messages/index.html"
			controller: "MessagesController"

		.state "messages.new",
			url: "/new"
			views:
				"@":
					templateUrl: "boardroom/partials/messages/create.html"
					controller: "MessagesNewController"