# The conferences submodule
app = angular.module("boardroom.conferences", [
	"ui.router",
	"ui.bootstrap",
	"resources",

	"conference.services"
	"conference.whiteboard.directive"
])


# ConferencesController
# This is the meeting room landing page.
app.controller "ConferencesController", ($scope)->




# ConferencesRunningController
# This shows all the conferences that the user has been invited to and is currently running.
app.controller "ConferencesRunningController", ($scope, $state, Conference, User, Contact)->
	$scope.user = User.get "me"
	$scope.contacts = Contact.query {status: "ACCEPTED"}
	$scope.conferences = Conference.invites()

	$scope.get_contact = (id)->
		return $scope.user.full_name if id == $scope.user.id

		for contact in $scope.contacts
			return contact.name if contact.id == id


	$scope.join = (conference)->
		Conference.join(conference.id).then ()->
			$state.go "conferences.room"




# ConferencesNewController
# This creates a new conference and invites contacts to join.
app.controller "ConferencesNewController", ($scope, Conference, Contact)->
	$scope.contacts = Contact.query {status: "ACCEPTED"}
	$scope.forms =
		contacts:
			invited: []


	$scope.invite = ()->
		$scope.forms.contacts.invited.push $scope.forms.contacts.current
		$scope.forms.contacts.current = ""


	$scope.create = ()->
		contacts = (contact.id for contact in $scope.forms.contacts.invited)

		Conference.create($scope.forms.name, contacts).then (id)->
			Conference.join id


	# conference events
	$scope.$on "conference.invite", (event, invite)->
		console.log invite



# ConferenceWhiteboardController
# A shared whiteboard for everyone who joined the conference.
app.controller "ConferencesWhiteboardController", ($scope, Conference, Contact)->
	$scope.contacts = Contact.query {status: "ACCEPTED"}



# ConferenceRoomController
# The primary room for conferences. Shows video of everyone who joined the conference.
app.controller "ConferencesRoomController", ($scope, $sce, UserMedia, RTCService, Conference, Contact)->
	$scope.conference = Conference.current
	$scope.contacts = Contact.query {status: "ACCEPTED"}

	$scope.stream = null
	$scope.remote_stream = null

	UserMedia.get_stream().then (stream)->
		url = window.URL.createObjectURL stream
		$scope.stream = $sce.trustAsResourceUrl url

		RTCService.set_stream stream

		RTCService.connect(2).then (conn)->
			console.log "promise", conn


	$scope.$on "rtc.connected", (ev, id, conn)->
		console.log "broadcast", id, conn


	$scope.$on "rtc.stream_added", (ev, id, stream)->
		console.log "new stream", id, stream

		url = window.URL.createObjectURL stream
		$scope.remote_stream = $sce.trustAsResourceUrl url




# Conferences Routing
# The various routes and states for the conferences submodule
app.config ($stateProvider, $urlRouterProvider)->
	$stateProvider
		.state "conferences",
			abstract: true
			url: "/conferences"
			templateUrl: "boardroom/partials/conferences/index.html"
			controller: "ConferencesController"

		.state "conferences.running",
			url: "/running"
			templateUrl: "boardroom/partials/conferences/running.html"
			controller: "ConferencesRunningController"

		.state "conferences.new",
			url: "/new"
			templateUrl: "boardroom/partials/conferences/new.html"
			controller: "ConferencesNewController"

		.state "conferences.whiteboard",
			url: "/whiteboard"
			templateUrl: "boardroom/partials/conferences/whiteboard.html"
			controller: "ConferencesWhiteboardController"

		.state "conferences.room",
			url: "/room"
			templateUrl: "boardroom/partials/conferences/room.html"
			controller: "ConferencesRoomController"