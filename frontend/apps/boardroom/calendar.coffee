# The calendar submodule
app = angular.module("boardroom.calendar", [
	"ui.router",
	"ui.calendar",
	"resources",
])


# CalendarController
# This is the calendar landing page which shows a full screen calendar.
app.controller "CalendarController", ($scope, $http, Event)->
	boardroom_source =
		events: []
		textColor: "white"

	$scope.current = {}
	$scope.eventSources = [boardroom_source]

	$scope.events = Event.query {}, (response)->
		for object in response.data.objects
			boardroom_source.events.push
				title: object.title
				start: object.start
				end: object.end


	title_month = (date)->
		date.format "MMMM YYYY"

	title_day = (date)->
		date.format "dddd, MMM Do, YYYY"

	title_week = (start, end)->
		if start.month() == end.month()
			start = start.format "MMM Do"
			end = end.format "Do, YYYY"

		else if start.year() == end.year()
			start = start.format "MMM Do"
			end = end.format "MMM Do, YYYY"

		else
			start = start.format "MMM Do, YYYY"
			end = end.format "MMM Do, YYYY"

		"#{start} - #{end}"


	$scope.config =
		calendar:
			editable: true
			header: false
			aspectRatio: 1.5

			viewRender: (view, element)->
				start = moment(view.start)
				end = moment(view.end) if view.end?

				$scope.current.title = title_month(start) if view.name == "month"
				$scope.current.title = title_week(start, end) if view.name == "agendaWeek"
				$scope.current.title = title_day(start) if view.name == "agendaDay"



	$scope.monthly = ()->
		$scope.calendar.fullCalendar "changeView", "month"

	$scope.weekly = ()->
		$scope.calendar.fullCalendar "changeView", "agendaWeek"

	$scope.daily = ()->
		$scope.calendar.fullCalendar "changeView", "agendaDay"


	$scope.next = ()->
		$scope.calendar.fullCalendar "next"

	$scope.prev = ()->
		$scope.calendar.fullCalendar "prev"

	$scope.today = ()->
		$scope.calendar.fullCalendar "today"


	$scope.connect_google = ()->
		window.open "/calendar/connect", "Connect Account", "width=800,height=600,modal=yes,alwaysRaised=yes"


	$scope.sync_google = ()->
		# why POST? because it has side-effects making GET unsuitable
		$http.post("/calendar/sync", {})
			.success (response)->
				console.log response





# Calendar Routing
# The various routes and states for the calendar submodule
app.config ($stateProvider, $urlRouterProvider)->
	$stateProvider
		.state "calendar",
			url: "/calendar",
			templateUrl: "boardroom/partials/calendar/index.html",
			controller: "CalendarController"