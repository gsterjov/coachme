# The people submodule
app = angular.module("boardroom.people", [
	"ui.router",
	"resources",
])


# PeopleController
# This is the people landing page which shows a list of contacts.
app.controller "PeopleController", ($scope, Contact)->
	$scope.contacts = Contact.query {}
	$scope.processing = false

	$scope.accept = (contact)->
		$scope.processing = true

		resource = Contact
		resource.id = contact.id
		resource.status = "ACCEPTED"
		resource.put().then ()->
			contact.status = "ACCEPTED"
			$scope.processing = false

	$scope.decline = (contact)->
		$scope.processing = true

		resource = Contact
		resource.id = contact.id
		resource.status = "DECLINED"
		resource.put().then (response)->
			contact.status = "DECLINED"
			$scope.processing = false


	$scope.descriptive_status = (status)->
		switch status
			when "PENDING" then "The user has requested to add you as a contact"



# PeopleAddController
# Add a person to the contact list
app.controller "PeopleAddController", ($scope, $state, Contact)->
	$scope.contact = Contact
	$scope.errors = {}

	$scope.submit = ()->
		$scope.contact.post()
			.success (response)->
				$state.go "people"
			.error (response)->
				$scope.errors = response




# People Routing
# The various routes and states for the people submodule
app.config ($stateProvider, $urlRouterProvider)->
	$stateProvider
		.state "people",
			url: "/people",
			templateUrl: "boardroom/partials/people/index.html",
			controller: "PeopleController"

		.state "people.add",
			url: "/add"
			views:
				"@":
					templateUrl: "boardroom/partials/people/add.html",
					controller: "PeopleAddController"