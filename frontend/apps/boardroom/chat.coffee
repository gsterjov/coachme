app = angular.module("boardroom.chat", ["chat"])

app.controller "ChatController", ($scope, Chat)->
	$scope.show_contacts = false
	$scope.active_contacts = []

	$scope.$on "chat.message", (event, sender, message, ts)->
		$scope.active_contacts.push sender if sender not in $scope.active_contacts

	$scope.toggle_contacts = ()->
		$scope.show_contacts = !$scope.show_contacts