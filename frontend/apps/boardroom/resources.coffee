# The resources service
# All boardroom resources should be defined in this service
service = angular.module("resources", ["core.resources"])


service.factory "Project", (Resource)->
	Resource
		url: "/api/boardroom/project/"
		cache: true


service.factory "Brief", (Resource)->
	Resource
		url: "/api/boardroom/brief/"
		cache: true


service.factory "Milestone", (Resource)->
	Resource
		url: "/api/boardroom/milestone/"
		cache: true


service.factory "Task", (Resource)->
	Resource
		url: "/api/boardroom/task/"
		cache: true


service.factory "Message", (Resource)->
	Resource
		url: "/api/messages/message/"
		cache: true


service.factory "Event", (Resource)->
	Resource
		url: "/api/events/event/"
		cache: true