# The project submodule
app = angular.module("boardroom.projects", [
	"ui.router",
	"resources",
])


# ProjectsController
# This is the projects landing page which shows a list of projects.
app.controller "ProjectsController", ($scope, Project)->
	$scope.projects = Project.query()



# ProjectsNewController
# The controller to create new projects
app.controller "ProjectsNewController", ($scope, $state, Project)->
	$scope.project = Project
	$scope.processing = false

	$scope.submit = ()->
		$scope.processing = true

		$scope.project.post()
			.success (response)->
				$state.go "projects"
				$scope.processing = false
			.error (response)->
				$scope.errors = response.data
				$scope.processing = false



# ProjectOverviewController
# The controller that shows an overview of the currently selected project
app.controller "ProjectOverviewController", ($scope, $state, Project, User)->
	$scope.owner = {}

	$scope.project = Project.get $state.params.id, (response)->
		$scope.owner = User.get response.data.owner



# ProjectBriefController
# Each project must have a brief which is created by the coach. Anyone can add notes to the brief.
app.controller "ProjectBriefController", ($scope, $state, Project, Brief)->
	$scope.brief = {}

	$scope.project = Project.get $state.params.id, (response)->
		$scope.brief = Brief.get response.data.brief



# ProjectMilestonesController
# The controller to show and create new milestones, which appear in their event calendar
app.controller "ProjectMilestonesController", ($scope, $state, Project, Milestone)->
	$scope.project = Project.get $state.params.id
	$scope.milestones = Milestone.query project: $state.params.id
	$scope.milestone = Milestone
	$scope.processing = false

	$scope.submit = ()->
		$scope.processing = true

		$scope.milestone.project = $scope.project
		$scope.milestone.post()
			.success (respone)->
				$state.go "projects.detail.milestones"
				$scope.processing = false
			.error (response)->
				$scope.errors = response.data
				$scope.processing = false



# ProjectTasksController
# The controller to show and create new tasks, which appear in their event calendar
app.controller "ProjectTasksController", ($scope, $state, Project, Task)->
	$scope.project = Project.get $state.params.id
	$scope.tasks = Task.query project: $state.params.id
	$scope.task = Task
	$scope.processing = false

	$scope.submit = ()->
		$scope.processing = true

		$scope.task.project = $scope.project
		$scope.task.post()
			.success (respone)->
				$state.go "projects.detail.tasks"
				$scope.processing = false
			.error (response)->
				$scope.errors = response.data
				$scope.processing = false




# ProjectFilesController
# The controller to collect and add new files associated with the project
app.controller "ProjectFilesController", ($scope, $state, Project)->
	$scope.project = Project.get $state.params.id





# Project Routing
# The various routes and states for the projects submodule
app.config ($stateProvider, $urlRouterProvider)->
	$stateProvider
		.state "projects",
			url: "/projects",
			templateUrl: "boardroom/partials/projects.html",
			controller: "ProjectsController"

		.state "projects.new",
			url: "/new",
			views:
				"@":
					templateUrl: "boardroom/partials/projects/new.html",
					controller: "ProjectsNewController"

		.state "projects.detail",
			abstract: true,
			url: "/:id",
			views:
				"@":
					templateUrl: "boardroom/partials/projects/index.html",
					controller: ()->

		.state "projects.detail.overview",
			url: "/overview",
			templateUrl: "boardroom/partials/projects/overview.html",
			controller: "ProjectOverviewController"

		.state "projects.detail.brief",
			url: "/brief",
			templateUrl: "boardroom/partials/projects/brief.html",
			controller: "ProjectBriefController"

		.state "projects.detail.milestones",
			url: "/milestones",
			templateUrl: "boardroom/partials/projects/milestones.html",
			controller: "ProjectMilestonesController"

		.state "projects.detail.milestones.new",
			url: "/new",
			views:
				"@projects.detail":
					templateUrl: "boardroom/partials/projects/milestones_new.html",
					controller: "ProjectMilestonesController"

		.state "projects.detail.tasks",
			url: "/tasks",
			templateUrl: "boardroom/partials/projects/tasks.html",
			controller: "ProjectTasksController"

		.state "projects.detail.tasks.new",
			url: "/new",
			views:
				"@projects.detail":
					templateUrl: "boardroom/partials/projects/tasks_new.html",
					controller: "ProjectTasksController"

		.state "projects.detail.files",
			url: "/files",
			templateUrl: "boardroom/partials/projects/files.html",
			controller: "ProjectFilesController"