app = angular.module "conference.services", ["core.wamp.services", "resources"]


class ConferenceService
	constructor: (@$rootScope, @Wamp, User)->
		@current = joined: null

		# set uri shortcuts
		Wamp.prefix "conference", "http://coachme.com.au/conference#"

		# listen to the user's channel for any new events
		@user = User.get "me", ()=>
			Wamp.prefix "user-events", "http://coachme.com.au/conference/user/#{@user.id}/events#"

			Wamp.subscribe "user-events:invite", (uri, ev)=>
				@$rootScope.$broadcast "conference.invite", ev

		# find out if the user has already joined a conference
		Wamp.call("http://coachme.com.au/user#details").then (details)=>
			@join details.joined_conference if details? and details.joined_conference?



	# Create a new conference and invite the specified contacts
	create: (name, contacts)->
		@Wamp.call "conference:create", name, contacts


	# Get a list of conference invites
	invites: ()->
		@Wamp.call "conference:invites"


	# Join the specified conference
	join: (id)->
		@Wamp.call("conference:join", id).then (conference)=>
			@current.joined = conference

			@Wamp.prefix "conference-events", "http://coachme.com.au/conference/#{id}/events#"
			
			@Wamp.subscribe "conference-events:join", (uri, ev)=>
				@$rootScope.$broadcast "conference.join", ev

			@Wamp.subscribe "conference-events:leave", (uri, ev)=>
				@$rootScope.$broadcast "conference.leave", ev


	# Leave the specified conference
	leave: (id)->
		@Wamp.unsubscribe "conference-events:join"
		@Wamp.unsubscribe "conference-events:leave"
		@Wamp.call("conference:leave", id).then ()=>
			@current.joined = null




class WhiteboardService
	constructor: (@$rootScope, @Wamp) ->
		Wamp.call("http://coachme.com.au/user#details").then (details)=>
			if details
				Wamp.prefix "whiteboard-draw", "http://coachme.com.au/whiteboard/#{details.joined_conference}/events/draw#"

				Wamp.subscribe "whiteboard-draw:line_to", (uri, ev)=> @$rootScope.$broadcast "whiteboard.draw.line_to", ev
				Wamp.subscribe "whiteboard-draw:begin", (uri, ev)=> @$rootScope.$broadcast "whiteboard.draw.begin", ev
				Wamp.subscribe "whiteboard-draw:end", (uri, ev)=> @$rootScope.$broadcast "whiteboard.draw.end", ev


	begin_draw: (x, y) ->
		@Wamp.publish "whiteboard-draw:begin", {x: x, y: y}


	end_draw: () ->
		@Wamp.publish "whiteboard-draw:end"


	line_to: (x, y) ->
		@Wamp.publish "whiteboard-draw:line_to", {x: x, y: y}




# wrapping vendor prefixed functions
navigator.getMedia =
	navigator.getUserMedia ||
	navigator.webkitGetUserMedia ||
	navigator.mozGetUserMedia ||
	navigator.msGetUserMedia


class UserMediaService
	constructor: (@$q, @$rootScope)->
		@connection = $q.defer()
		@stream = null


	get_stream: ()->
		constraints =
			video: true
			audio: true

		navigator.getMedia constraints,
			(stream)=>
				@stream = stream
				@connection.resolve stream,
			(error)=>
				@stream = null
				@connection.reject error

		@connection.promise




# wrapping vendor prefixed classes
RTCPeerConnection =
	window.RTCPeerConnection ||
	window.mozRTCPeerConnection ||
	window.webkitRTCPeerConnection

RTCIceCandidate =
	window.RTCIceCandidate ||
	window.mozRTCIceCandidate ||
	window.webkitRTCIceCandidate

RTCSessionDescription =
	window.RTCSessionDescription ||
	window.mozRTCSessionDescription ||
	window.webkitRTCSessionDescription


class RTCService
	constructor: (@$q, @$rootScope, @Wamp, User)->
		# listen to the user's channel for any new candidates
		@user = User.get "me", ()=>
			Wamp.prefix "rtc-events", "http://coachme.com.au/rtc/#{@user.id}/events#"

			Wamp.subscribe "rtc-events:candidates", @on_candidate
			Wamp.subscribe "rtc-events:descriptors", @on_descriptor

		@config = iceServers: [url: "stun:stun.l.google.com:19302"]

		@defereds = {}
		@initiated = []
		@connections = {}
		@candidates = {}


	connect: (id)->
		defered = @$q.defer()
		@defereds[id] = defered

		@initiated.push id
		@create_connection id
		@create_descriptor id

		return defered.promise


	set_stream: (stream)->
		@stream = stream


	create_connection: (id)->
		# create and associate connection
		conn = new RTCPeerConnection @config
		@connections[id] = conn

		conn.addStream @stream

		# candidate handler
		conn.onicecandidate = (ev)=>
			if ev.candidate?
				@Wamp.publish "http://coachme.com.au/rtc/#{id}/events#candidates",
					id: ev.candidate.sdpMid
					index: ev.candidate.sdpMLineIndex
					candidate: ev.candidate.candidate

		# event handlers
		conn.onaddstream = (ev)=>
			@$rootScope.$broadcast "rtc.stream_added", id, ev.stream

		conn.onremovestream = ()-> console.log "remove stream"

		conn.oniceconnectionstatechange = (ev)=>
			console.log conn.iceConnectionState

			if conn.iceConnectionState == "connected"
				@defereds[id].resolve conn if @defereds[id]?
				@$rootScope.$broadcast "rtc.connected", id, conn

		return conn


	create_descriptor: (id)->
		conn = @connections[id]

		# session descriptor handler
		desc_handler = (desc)=>
			conn.setLocalDescription desc
			@Wamp.publish "http://coachme.com.au/rtc/#{id}/events#descriptors", desc

		error_handler = (error)->
			console.log error

		# caller. create local SDP and send it to the contact
		if id in @initiated
			conn.createOffer desc_handler, error_handler

		# callee. create answer and send it to the caller
		else
			conn.createAnswer desc_handler, error_handler


	on_descriptor: (uri, ev)=>
		conn = @connections[ev.user]
		conn = @create_connection ev.user if not conn?

		desc = new RTCSessionDescription ev
		conn.setRemoteDescription desc, ()=>
			# add cached candidates
			if @candidates[ev.user]
				conn.addIceCandidate candidate for candidate in @candidates[ev.user]
				@candidates[ev.user] = []

			@create_descriptor ev.user if not conn.localDescription? and ev.user not in @initiated


	on_candidate: (uri, ev)=>
		conn = @connections[ev.user]
		conn = @create_connection ev.user if not conn?

		candidate = new RTCIceCandidate
			sdpMid: ev.id
			sdpMLineIndex: ev.index
			candidate: ev.candidate
		
		if conn.remoteDescription?
			conn.addIceCandidate candidate
		else
			@candidates[ev.user] ||= []
			@candidates[ev.user].push candidate





app.service "Whiteboard", ["$rootScope", "Wamp", WhiteboardService]
app.service "Conference", ["$rootScope", "Wamp", "User", ConferenceService]
app.service "UserMedia", ["$q", "$rootScope", UserMediaService]
app.service "RTCService", ["$q", "$rootScope", "Wamp", "User", RTCService]