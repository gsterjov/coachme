app = angular.module("conference.whiteboard.directive", ["conference.services"])


class CanvasLayer

	constructor: (@context) ->



	apply_style: (style) ->
		@context.strokeStyle = style.colour if style.colour


	fill_background: (colour) ->
		@context.fillStyle = colour
		@context.fillRect 0, 0, @canvas.width(), @canvas.height()


	begin_draw: (x, y, style) ->
		@is_drawing = true
		if style? then @apply_style style

		@context.beginPath()
		@context.moveTo x, y


	end_draw: () ->
		@is_drawing = false


	line_to: (x, y, style) ->
		if @is_drawing
			if style? then @apply_style style
			@context.lineTo x, y
			@context.stroke()



app.directive "whiteboard", ["Whiteboard", (Whiteboard)->
	link: (scope, element, attrs)->
		# create canvas
		context = element[0].getContext "2d"
		layer = new CanvasLayer context
		drawing = false

		# events
		element.on "mouseup", (ev)->
			drawing = false
			layer.end_draw()
			Whiteboard.end_draw()

		element.on "mousedown", (ev)->
			drawing = true
			[x, y] = [ev.offsetX, ev.offsetY]

			layer.begin_draw x, y
			Whiteboard.begin_draw x, y

		element.on "mousemove", (ev)->
			if drawing
				[x, y] = [ev.offsetX, ev.offsetY]
				layer.line_to x, y
				Whiteboard.line_to x, y


		element.on "$destroy", ()->
			element.off()


		# whiteboard events
		scope.$on "whiteboard.draw.line_to", (ev, data)-> layer.line_to data.x, data.y
		scope.$on "whiteboard.draw.begin", (ev, data)-> layer.begin_draw data.x, data.y
		scope.$on "whiteboard.draw.end", (ev, data)-> layer.end_draw()
]