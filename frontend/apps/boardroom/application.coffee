app = angular.module("boardroom", [
	"ui.router",

	"templates",
	"core.settings",
	"boardroom.chat",
	"boardroom.calendar",
	"boardroom.messages",
	"boardroom.projects",
	"boardroom.people",
	"boardroom.conferences",
])


app.config ($stateProvider, $urlRouterProvider)->
	$urlRouterProvider.otherwise "/dashboard"

	$stateProvider
		.state "dashboard",
			url: "/dashboard",
			templateUrl: "boardroom/partials/dashboard.html",
			controller: ($scope)->



app.config ($httpProvider)->
	$httpProvider.defaults.xsrfCookieName = "csrftoken"
	$httpProvider.defaults.xsrfHeaderName = "X-CSRFToken"



app.config (WampProvider, Settings)->
	host = Settings.realtime.host
	port = Settings.realtime.port
	WampProvider.set_uri "ws://#{host}:#{port}"