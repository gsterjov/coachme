# The resources service
# All jobs resources should be defined in this service
service = angular.module("jobs.resources", ["core.resource"])


service.factory "Job", (Resource)->
	Resource
		url: "/api/jobs/job/"
		cache: true



service.factory "Proposal", (Resource)->
	Resource
		url: "/api/jobs/proposal/"
		cache: true