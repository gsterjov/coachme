app = angular.module("jobs.job", [
	"ui.bootstrap",
	"Kalendae",

	"jobs.resources",
	"coaches.resources",
])


app.controller "CreateJobController", ($scope, $state, Job, Skill, Industry)->
	$scope.job = Job
	$scope.skills = Skill.query()
	$scope.industries = Industry.query()
	$scope.forms =
		skills: {}


	$scope.add = (form_name)->
		form = $scope.forms[form_name]
		$scope.job.skills ||= []
		$scope.job.skills.push name: form.current
		form.current = ""


	$scope.create = ()->
		$scope.job.duration = $scope.forms.duration if $scope.forms.duration?

		$scope.job.post()
			.error (response)->
				$scope.errors = response



app.controller "JobListController", ($scope, $state, User, Job)->
	$scope.user = User.get "me"
	$scope.jobs = Job.query()



app.controller "JobProposalsController", ($scope, $state, Job, Proposal)->
	$scope.job = Job.get $state.params.id
	$scope.proposals = Proposal.query()



app.controller "FindJobController", ($scope, $state, User, Job)->
	$scope.user = User.get "me"
	$scope.job = null
	$scope.jobs = Job.queryURL "/api/jobs/job/recommended/"
	$scope.applying = false;


	$scope.apply = (job)->
		$scope.job = job
		$scope.applying = true

	$scope.cancel = ()->
		$scope.applying = false



app.controller "CreateProposalController", ($scope, Proposal)->
	$scope.proposal = Proposal
	$scope.forms = {}

	$scope.submit = ()->
		$scope.proposal.job = $scope.job
		$scope.proposal.duration = $scope.forms.duration

		$scope.proposal.post()
			.error (response)->
				$scope.errors = response