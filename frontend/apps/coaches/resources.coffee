# The resources service
# All coach resources should be defined in this service
service = angular.module("coaches.resources", ["core.resource"])


service.factory "Coach", (Resource)->
	Resource
		url: "/api/coaches/coach/"
		cache: true


service.factory "Skill", (Resource)->
	Resource
		url: "/api/coaches/skill/"
		cache: true


service.factory "Industry", (Resource)->
	Resource
		url: "/api/coaches/industry/"
		cache: true


service.factory "Testimonial", (Resource)->
	Resource
		url: "/api/coaches/testimonial/"
		cache: true