app = angular.module("coaches.register", [
	"ui.bootstrap",

	"core.services",
	"coaches.resources",
])


app.controller "RegisterController", ($scope, $state, Session, Coach, Skill, Industry)->
	$scope.skills = Skill.query()
	$scope.industries = Industry.query()

	$scope.step = parseInt($state.params.step)
	$scope.coach = Coach
	$scope.form = {}
	$scope.errors = {}


	create_coach = ()->
		$scope.coach.states = []
		$scope.coach.industries = []
		$scope.coach.skills = []

		$scope.coach.post()
			.success (response)->
				# automatically log in as the new coach
				Session.login($scope.coach.email, $scope.coach.password)
					.success ()->
						$scope.coach = Coach.get "me"
						next_step()

			.error (response)->
				$scope.errors = response


	save_coach = ()->
		$scope.coach.put()
			.success (response)->
				$state.go "profile"
			.error (response)->
				$scope.errors = response


	next_step = ()->
		++$scope.step
		$state.go "register.step", step: $scope.step


	$scope.add = (form_name)->
		$scope.coach[form_name].push name: $scope.form[form_name]
		$scope.form[form_name] = ""


	$scope.next = ()->
		if $scope.step == 1
			create_coach()
		else if $scope.step == 2
			save_coach()


	$scope.prev = ()->
		--$scope.step
		$state.go "register.step", step: $scope.step


	$scope.$on "$stateChangeSuccess", (event, toState, toParams, fromState, fromParams)->
		$scope.step = parseInt(toParams.step)
		if fromState.data? and fromState.data.form
			$scope.coach.first_name = fromState.data.form.first_name || ""
			$scope.coach.email = fromState.data.form.email || ""