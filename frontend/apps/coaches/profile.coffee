app = angular.module("coaches.profile", [
	"ngCookies"
	"ui.bootstrap",
	"imageupload",

	"coaches.resources",
])


app.controller "ProfileController", ($scope, $cookies, $state, Coach, Skill, Industry, Testimonial)->
	$scope.photo = null
	$scope.coach = Coach.get $state.params.id || "me"
	$scope.skills = Skill.query()
	$scope.industries = Industry.query()
	$scope.forms =
		about_me: {}
		photo: {}
		skills: {}
		industries: {}
		testimonials: values: [{}, {}, {}] # max three testimonials


	$scope.edit = (form_name)->
		form = $scope.forms[form_name]
		form.value = $scope.coach[form_name]
		form.editing = true


	$scope.save = (form_name)->
		form = $scope.forms[form_name]

		$scope.coach[form_name] = form.value
		$scope.coach.put().then ()->
			form.editing = false


	$scope.discard = (form_name)->
		$scope.forms[form_name].editing = false



	$scope.add = (form_name)->
		form = $scope.forms[form_name]
		form.selected ||= []
		form.selected.push name: form.current
		form.current = ""


	$scope.save_list = (form_name)->
		form = $scope.forms[form_name]

		$scope.coach[form_name] = form.selected
		$scope.coach.put().then ()->
			form.selected = []
			form.editing = false


	$scope.edit_testimonials = ()->
		form = $scope.forms.testimonials
		form.values = $scope.coach.testimonials
		form.editing = true

	$scope.save_testimonials = ()->
		form = $scope.forms.testimonials

		$scope.coach.testimonials = form.values
		$scope.coach.put().then ()->
			form.editing = false


	upload = (name, file, url)->
		data = new FormData()
		data.append name, file

		xhr = new XMLHttpRequest()
		xhr.upload.addEventListener "progress", (ev)->
			console.log (ev.loaded / ev.total) + "%"

		xhr.onreadystatechange = (ev)->
			# console.log ev

		xhr.open "PUT", url, true
		xhr.setRequestHeader "X-CSRFToken", $cookies.csrftoken
		xhr.send data


	$scope.$watch "photo", ()->
		if $scope.photo
			upload "photo", $scope.photo.file, $scope.coach.resource_uri