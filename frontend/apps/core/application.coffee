app = angular.module("core", [
	"ngCookies",
	"ui.router",
	"ui.bootstrap",

	"templates",
	"core.resources",
	"core.services",
	"core.search",
	"coaches.profile",
	"coaches.register",
	"jobs.job",
])


app.controller "NavigationController", ($scope, $modal, User, Session)->
	$scope.template = url: "core/partials/navigation.html"

	$scope.$on "session.user", (event)->
		$scope.user = Session.user
		type = if Session.user.is_coach then "coach" else "client"
		$scope.template = url: "core/partials/userbar_#{type}.html"

	$scope.login = ()->
		modal = $modal.open
			templateUrl: "core/partials/login.html"
			controller: "LoginController"



app.controller "LoginController", ($scope, $cookies)->
	$scope.csrftoken = $cookies.csrftoken



app.controller "HomeController", ($scope, $state, Coach)->
	$scope.form = {}
	$scope.stats = Coach.statistics()
	$scope.latest_coaches = Coach.latest()

	$scope.register = ()->
		$state.current.data.form = $scope.form
		$state.go "register.step", step: 1

	$scope.search = ()->
		$state.go "search", q: $scope.form.search



app.config ($stateProvider, $urlRouterProvider)->
	$urlRouterProvider.otherwise "/home"

	$stateProvider
		.state "home",
			url: "/home"
			templateUrl: "core/partials/home.html"
			controller: "HomeController"
			data:
				form: {}

		.state "search",
			url: "/search?q"
			templateUrl: "core/partials/search.html"
			controller: "SearchController"

		.state "profile",
			url: "/profile/:id"
			templateUrl: "coaches/partials/profile.html"
			controller: "ProfileController"

		.state "register",
			abstract: true
			url: "/coach/register"
			templateUrl: "coaches/partials/register.html"
			controller: "RegisterController"

		.state "register.step",
			url: "/step{step}"
			templateUrl: (params)-> "coaches/partials/register_step#{params.step}.html"
			controller: ()->

		.state "jobs",
			abstract: true
			url: "/jobs"
			template: "<div ui-view></div>"

		.state "jobs.create",
			abstract: true
			url: "/create"
			templateUrl: "jobs/partials/create.html"
			controller: "CreateJobController"

		.state "jobs.create.step",
			url: "/step{step}"
			templateUrl: (params)-> "jobs/partials/create_step#{params.step}.html"
			controller: ()->

		.state "jobs.list",
			url: "/list"
			templateUrl: "jobs/partials/list.html"
			controller: "JobListController"

		.state "jobs.find",
			url: "/find"
			templateUrl: "jobs/partials/find.html"
			controller: "FindJobController"

		.state "jobs.proposals",
			url: "/:id/proposals"
			templateUrl: "jobs/partials/proposals.html"
			controller: "JobProposalsController"



app.config ($httpProvider)->
	$httpProvider.defaults.xsrfCookieName = "csrftoken"
	$httpProvider.defaults.xsrfHeaderName = "X-CSRFToken"