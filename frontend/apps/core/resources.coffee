# The resources service
# All core resources should be defined in this service
service = angular.module("core.resources", ["core.resource"])


service.factory "User", (Resource)->
	Resource
		url: "/api/core/user/"
		cache: true


service.factory "Contact", (Resource)->
	Resource
		url: "/api/core/contact/"
		cache: true


service.factory "Relationship", (Resource)->
	Resource
		url: "/api/core/relationship/"
		cache: true




# Subclass the tastypie resource factory so we
# can implement custom features
class ResourceFactory extends TastyResourceFactory

	search: (params, success, error)->
		@queryURL "#{@_config.url}search/", params, success, error

	statistics: (params, success, error)->
		@queryURL "#{@_config.url}statistics/", params, success, error

	latest: (params, success, error)->
		@queryURL "#{@_config.url}latest/", params, success, error


module = angular.module("core.resource", ["tastyResource"])

module.factory "Resource", ($http)->
	(config)->
		new ResourceFactory($http, config)