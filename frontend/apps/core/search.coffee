app = angular.module("core.search", [
	"ui.bootstrap",

	"coaches.resources",
])


app.controller "SearchController", ($scope, $state, Coach)->
	$scope.form = search: $state.params.q
	$scope.coaches = Coach.search q: $state.params.q

	$scope.search = ()->
		$state.go "search", q: $scope.form.search