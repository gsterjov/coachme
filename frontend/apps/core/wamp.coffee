app = angular.module "core.wamp.services", []


# A service that interacts with a wamp server.
#
# This is a low level service to manage the session, make RPC calls and
# handle pub/sub tasks. It does this in an angular friendly way (promises, etc.)
# Its best to create higher level services that uses WampService but abstracts away
# the event hooking and function calls.
#
# The API naming convention is identical to AutobahnJS for convenience.
class WampService
	constructor: (@$q, @$rootScope, @uri)->
		@connection = $q.defer()
		@connect()


	# Connect to the service
	connect: () ->
		@$q.when ab.connect(@uri,
			(@session)=> @connection.resolve @session,
			(code, reason)=> @connection.reject()
		)


	# Register a URI prefix
	prefix: (prefix, uri)->
		@connection.promise.then ()=>
			@session.prefix prefix, uri


	# Make an RPC request
	call: (uri, args...)->
		@connection.promise.then ()=>
			@$q.when @session.call(uri, args...)


	# Publish to a topic
	publish: (uri, args...)->
		@connection.promise.then ()=>
			@$q.when @session.publish(uri, args...)


	# Subscribe to a topic
	subscribe: (uri, handler)->
		@connection.promise.then ()=>
			@$q.when @session.subscribe(uri, handler)


	# Unsubscribe to a topic
	unsubscribe: (uri, handler)->
		@connection.promise.then ()=>
			@$q.when @session.unsubscribe(uri, handler)




app.provider "Wamp", ()->
	uri = "ws://localhost:9000"

	@set_uri = (val)->
		uri = val

	@$get = ($q, $rootScope)->
		new WampService $q, $rootScope, uri

	return this