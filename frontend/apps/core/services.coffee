service = angular.module "core.services", ["core.resources"]


class SessionService
	constructor: (@$rootScope, @$cookies, @$http, @User)->
		@_fetch_user()


	# Login with the supplied credential and notify of any success/failure
	login: (email, password)->
		req = @$http.post "/api/core/user/login/",
			email: email
			password: password

		req.success (response)=>
			@_fetch_user()


	_fetch_user: ()->
		@user = @User.get "me", (response)=>
			@$rootScope.$broadcast "session.user"



service.service "Session", ["$rootScope", "$cookies", "$http", "User", SessionService]