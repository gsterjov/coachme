gulp = require ("gulp")
util = require ("gulp-util")
clean = require ("gulp-clean")
rename = require ("gulp-rename")
size = require ("gulp-filesize")

bower = require ("gulp-bower-files")

less = require ("gulp-less")
coffee = require ("gulp-coffee")

ngmin = require ("gulp-ngmin")
templatecache = require ("gulp-angular-templatecache")

concat = require ("gulp-concat")
uglify = require ("gulp-uglify")
minifycss = require ("gulp-minify-css")
minifyhtml = require ("gulp-minify-html")



var is_dev = util.env.type !== "production"



gulp.task ("scripts-bower", function() {
	var combined = util.combine (
		bower(),
		gulp.dest ("dist/lib")
	)

	return combined()
})


gulp.task ("scripts-common", ["scripts-bower"], function() {
	var combined = util.combine (
		gulp.src ([
			"dist/lib/**/*.js",

			"apps/lib/autobahn.js",
			"apps/lib/kalendae.js",
			"apps/lib/tasty-resource.js",
			"apps/lib/angular-imageupload.js",
			"apps/lib/angular-kalendae.js",
			"apps/lib/angular-ui-calendar.js"
		]),
		ngmin(),
		concat ("common.js"),
		// !is_dev ? uglify() : util.noop(),
		gulp.dest ("dist/"),
		size()
	)

	return combined()
})


gulp.task ("scripts-calendar", function() {
	var combined = util.combine (
		gulp.src ([
			"apps/lib/fullcalendar/jquery.min.js",
			"apps/lib/fullcalendar/jquery-ui.custom.min.js",
			"apps/lib/fullcalendar/fullcalendar.min.js"
		]),
		concat ("calendar.js"),
		gulp.dest ("dist/")
	)

	return combined()
})


gulp.task ("scripts-core", function() {
	var combined = util.combine (
		gulp.src ([
			"apps/core/*.coffee",
			"apps/coaches/*.coffee",
			"apps/jobs/*.coffee",

			is_dev ? "!apps/**/*.production.coffee" : "!apps/**/*.local.coffee"
		]),
		coffee(),
		ngmin(),
		concat ("core.js"),
		// !is_dev ? uglify() : util.noop(),
		gulp.dest ("dist/"),
		size()
	)

	return combined()
})

gulp.task ("scripts-boardroom", function() {
	var combined = util.combine (
		gulp.src ("apps/boardroom/**/*.coffee"),
		coffee(),
		ngmin(),
		concat ("boardroom.js"),
		// !is_dev ? uglify() : util.noop(),
		gulp.dest ("dist/"),
		size()
	)

	return combined()
})


gulp.task ("templates", function() {
	var combined = util.combine (
		gulp.src ("apps/**/*.html"),
		minifyhtml({empty: true}),
		templatecache({standalone: true}),
		gulp.dest ("dist/"),
		size()
	)

	return combined()
})



gulp.task("styles-common", function() {
	var combined = util.combine (
		gulp.src ([
			"styles/lib/bootstrap/bootstrap.less",
			"styles/lib/animate-custom.css",
			"styles/lib/kalendae.css",
			"styles/lib/fullcalendar.css",
		]),
		less(),
		concat ("common.css"),
		minifycss(),
		gulp.dest ("dist/")
	)

	return combined()
});

gulp.task("styles-core", function() {
	var combined = util.combine (
		gulp.src ([
			"styles/core.less",
			"styles/home.less",
			"styles/profile.less",
			"styles/jobs.less",
		]),
		less(),
		concat ("core.css"),
		minifycss(),
		gulp.dest ("dist/")
	)

	return combined()
});

gulp.task("styles-boardroom", function() {
	var combined = util.combine (
		gulp.src ([
			"styles/boardroom.less",
			"styles/calendar.less",
			"styles/projects.less",
			"styles/conferences.less",
		]),
		less(),
		concat ("boardroom.css"),
		minifycss(),
		gulp.dest ("dist/")
	)

	return combined()
});




gulp.task ("watch", function() {
	gulp.watch ("apps/**/*.coffee", ["scripts-core", "scripts-boardroom"])
	gulp.watch ("styles/*.less", ["styles-core", "styles-boardroom"])
	gulp.watch ("apps/**/*.html", ["templates"])
})


gulp.task ("clean", function() {
	return gulp.src ("dist/", {read: false}).pipe (clean())
})


gulp.task ("scripts", ["scripts-common", "scripts-core", "scripts-boardroom", "scripts-calendar"])
gulp.task ("styles", ["styles-common", "styles-boardroom", "styles-core"])

gulp.task ("default", ["scripts", "styles", "templates"])